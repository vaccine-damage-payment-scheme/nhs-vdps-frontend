# # tag latest version after cluster has started so this version is not picked up
# echo "Docker login  ..."
# aws ecr get-login-password --region $AWS_DEFAULT_REGION  --endpoint $END_POINT | docker login --username AWS --password-stdin $DOCKER_REGISTRY

# echo "Building Image ..."
# docker build -t $IMAGE_NAME:$IMAGE_TAG .

# echo "Tagging Image ..."
# docker tag $IMAGE_NAME:$IMAGE_TAG $DOCKER_REGISTRY/$IMAGE_NAME:$IMAGE_TAG

echo "Version release now"