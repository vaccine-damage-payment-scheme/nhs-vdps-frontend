aws sts get-caller-identity
echo "Docker login  ..."
aws ecr get-login-password --region $AWS_DEFAULT_REGION  --endpoint $END_POINT | docker login --username AWS --password-stdin $AWS_ACCOUNT_ID$DOCKER_REGISTRY
echo $AWS_ACCOUNT_ID$DOCKER_REGISTRY
echo "Deploying Image to ecr latest"
docker push $AWS_ACCOUNT_ID$DOCKER_REGISTRY/$IMAGE_NAME:$IMAGE_TAG

echo "Deploying Image to ecr ..."
# VERSION_NO=$CI_COMMIT_REF_NAME$(cat ./version)
VERSION_NO=v$(cat ./version)
docker push $AWS_ACCOUNT_ID$DOCKER_REGISTRY/$IMAGE_NAME:$VERSION_NO

echo "List existing Service Task ..."
TASK=$(aws ecs list-tasks --cluster $ECS_CLUSTER --output text)

if [  -z "${TASK}"  ]
   then
   echo "First deployment - nothing to terminate - spin up new task"
else
   echo $TASK
   TASK=$(echo $TASK | tr 'TASKARNS' ' ')
   echo $TASK
   echo "Terminating existing task to recycle $TASK ..."
   aws ecs stop-task --cluster $ECS_CLUSTER --task $TASK
fi

echo "Run new task and check if running ..."
PENDING_TASK=""
while [  -z "${PENDING_TASK}"  ]
do 
   PENDING_TASK=$(aws ecs list-tasks --cluster $ECS_CLUSTER --desired-status RUNNING --output text)
   sleep 0.2
   echo "Restarting Service ..."
done

NEW_TASK=$(aws ecs list-tasks --cluster $ECS_CLUSTER --output text)


echo "New Cluster is up and running: $NEW_TASK"

echo sysctl -w fs.inotify.max_user_watches=1048576
echo sysctl -p