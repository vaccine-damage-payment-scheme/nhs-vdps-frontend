// Node.js core dependencies
const path = require('path')
const helmet =                  require("helmet");
var xss = require('xss')




// Npm dependencies
const express = require('express')
const favicon = require('serve-favicon')
const bodyParser = require('body-parser')
const logger = require('pino')()
const loggingMiddleware = require('morgan')
const argv = require('minimist')(process.argv.slice(2))
const staticify = require('staticify')(path.join(__dirname, 'public'))
const compression = require('compression')
var nunjucks = require('nunjucks')
const cookieParser = require('cookie-parser');
const { I18n } = require('i18n')
const i18n = new I18n()
const fileUpload = require('express-fileupload');


const config = require('./common/config')
const sessions = require('express-session')
let RedisStore =                require("connect-redis")(sessions)
const Redis = require("ioredis")
let redisClient = new Redis(process.env.REDIS_SERVER,{
                            showFriendlyErrorStack: true,
                            connectTimeout:100000,
                            keepAlive: 1,
                            commandTimeout(err){
                                console.log('commandTimeout,',err)
                            },
                          reconnectOnError(err) {
                            console.log('ERROR: ',err)
                            const targetError = "READONLY";
                            if (err.message.includes(targetError)) {
                              // Only reconnect when the error contains "READONLY"
                              return true; // or `return 1;`
                            }
                          },
                        });

redisClient.on('error', err => {
  console.log('REDIS: FAILED')
  console.log(err)
  process.exit(0)
})

redisClient.on('connect', () => {
    console.log('connected to REDIS')
  // continue program logic
})

// Local dependencies
const router = require('./app/router')

const noCache = require('./common/utils/no-cache')
const correlationHeader = require('./common/middleware/correlation-header')
const fs = require('fs')

const { logRequestStart } = require('./common/utils/request-logger')

// Global constants
const timeout = 1000 * 60 * 45
const unconfiguredApp = express()
const oneYear = 86400000 * 365
const publicCaching = { maxAge: oneYear }
const PORT = (process.env.PORT || 3000)
const { NODE_ENV } = process.env
const CSS_PATH = staticify.getVersionedPath('/stylesheets/application.min.css')
const JAVASCRIPT_PATH = staticify.getVersionedPath('/javascripts/application.js')

// Define app views
const APP_VIEWS = [
  path.join(__dirname, 'node_modules/nhsuk-frontend/packages/'),
  __dirname
]

function intializeExpress(app) {
  logRequestStart({
    level: 'info',
    message: `Initializing Express...`
  })
  app.disable('x-powered-by')
  app.set('settings', { getVersionedPath: staticify.getVersionedPath })

  // set favicon
  app.use(favicon(path.join(__dirname, 'public/assets/images/', 'favicon.ico')))
  // add compression
  app.use(compression())
  app.use(staticify.middleware)

  // add in security headers using helmet.
  app.use(helmet.crossOriginEmbedderPolicy());
  app.use(helmet.crossOriginOpenerPolicy());
  app.use(helmet.crossOriginResourcePolicy());
  app.use(helmet.dnsPrefetchControl());
  app.use(helmet.expectCt());
  app.use(helmet.frameguard());
  app.use(helmet.hidePoweredBy());
  app.use(helmet.hsts());
  app.use(helmet.ieNoOpen());
  app.use(helmet.noSniff());
  app.use(helmet.originAgentCluster());
  app.use(helmet.permittedCrossDomainPolicies());
  app.use(helmet.referrerPolicy());
  app.use(helmet.xssFilter());
  app.use(
      helmet.contentSecurityPolicy({
        directives: {
          defaultSrc: ["self"]
        },
        reportOnly: true,
      })
  );

    app.use((req, res, next) => {
        res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        next();
    });


    app.disable('view cache');

    app.use(express.json());
    app.use(express.urlencoded({extended: true}));

    app.use(sessions({
        store: new RedisStore({client: redisClient}),
        secret: 'somevaluetest',
        saveUninitialized:true,
        cookie: { maxAge: timeout },
        resave: false
    }));

  app.use(function (req, res, next) {
    // check if client sent cookie
    var cookies = req.cookies
    if (cookies === undefined) {
      res.cookie('sessionID', req.sessionID, { maxAge: oneYear, httpOnly: true })
    } else {
      req.sessionID = req.cookies.sessionID
    }
    next() // <-- important!
  })
      // cookies
    app.use(cookieParser());

  if (process.env.DISABLE_REQUEST_LOGGING !== 'true') {
    app.use(/\/((?!images|public|stylesheets|javascripts).)*/, loggingMiddleware(
      ':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent" - total time :response-time ms'))
  }

  app.use(async function (req, res, next) {
    if(req.url !== '/') {// && !req.session.newVisitor
        if(!await redisClient.get('sess:'+req.sessionID)){
            req.session.timeout = 'common.errors.E0108'
            console.log('session timing out>!??!??!')
            res.redirect('/')
            return
        }
    }
    next()
  })

  app.use(fileUpload());
  app.use('*', correlationHeader)
  app.use('/javascripts', express.static(path.join(__dirname, '/public/assets/javascripts'), publicCaching))
  app.use('/images', express.static(path.join(__dirname, '/public/assets/images'), publicCaching))
  app.use('/stylesheets', express.static(path.join(__dirname, '/public/assets/stylesheets'), publicCaching))
  app.use('/public', express.static(path.join(__dirname, '/public')))
  app.use('/', express.static(path.join(__dirname, '/node_modules/nhsuk-frontend/')))

}

function setupNunjucks(app) {

  const nunjucksConfiguration = {
    express: app, // The express app that nunjucks should install to
    autoescape: true, // Controls if output with dangerous characters are escaped automatically
    throwOnUndefined: false, // Throw errors when outputting a null/undefined value
    trimBlocks: true, // Automatically remove trailing newlines from a block/tag
    lstripBlocks: true, // Automatically remove leading whitespace from a block/tag
    watch: NODE_ENV !== 'production', // Reload templates when they are changed (server-side). To use watch, make sure optional dependency chokidar is installed
    noCache: NODE_ENV !== 'production' // Never use a cache and recompile templates each time (server-side)
  }
  // Initialise nunjucks environment
  let env = nunjucks.configure(APP_VIEWS, nunjucksConfiguration)
  env.addGlobal("__", i18n.__)
  env.addFilter("t", i18n.__)
  env.addFilter('render', function (template, errors, data, locale) {
    const html = env.render(template, { errors, data, locale })
    return env.filters.safe(html) // try-catch
  })
  env.addFilter('getDigitFromDate', function (date, type, input) {
      if(input){
      return input
      }

      if(!date){
          return null
      }
      const parsed = date.split('-')
      if(type === 'day'){
        return (parsed[2] <= 0) ? null : (parsed[2].length < 2) ? `0${parsed[2]}` : parsed[2]
      }
      if(type === 'month'){
         return (parsed[1] <= 0) ? null : (parsed[1].length < 2) ? `0${parsed[1]}` : parsed[1]
      }
      if(type === 'year'){
          return (parsed[0] <= 0) ? null : parsed[0]
      }
    })

    env.addGlobal('keys',function(object) {
        return Object.keys(object)
    })

  env.addGlobal('parseErrors', function (errors, param) {
      if(errors){
        let error = errors.find(err => err.param === param) ? { text: errors.find(err => err.param === param)?.msg, locale: errors.find(err => err.param === param)?.locale } : null
        return error
      }
      return null
  })

  env.addGlobal('filterErrors', function (errors, param) {
    if(!errors){
      return null
    }
    const error = errors.filter(err => err.param === param)
    return error.length > 0 ? error : null;
  })

  env.addGlobal('checkContactPreferences', function (contactPreferences, type) {
    if(contactPreferences)
      return !!contactPreferences.find(cp => cp.includes(type))
    return false
  })
  env.addGlobal('renderContactPreferences', function (contactPreferences) {
    if(contactPreferences){
        return contactPreferences.map(cp => JSON.parse(cp).Name).join(', ')
    } else {
        return ''
    }
  })
  env.addGlobal('rf', function (date) {
    if(date){
        const splitDate = date.split('-');
            if(!splitDate[0] || !splitDate[1] || !splitDate[2]){
                return 'None provided'
            } else {
                const parsed = new Date(date)
              let day = parsed.getUTCDate()
              let month = parsed.getUTCMonth()+1
              const year = parsed.getFullYear()
              if(month < 10)
                month = `0${month}`
              if(day < 10)
                day = `0${day}`
              return `${day}/${month}/${year}`
            }
    }
    return null;

  })
  // Version static assets on production for better caching
  // if it's not production we want to re-evaluate the assets on each file change
  env.addGlobal('css_path', NODE_ENV === 'production' ? CSS_PATH : staticify.getVersionedPath('/stylesheets/application.min.css'))
//  env.addGlobal('js_path', NODE_ENV === 'production' ? JAVASCRIPT_PATH : staticify.getVersionedPath('/javascripts/application.js'))

  // filter to remove wanted characters from ID's
  env.addFilter("formatIdText", (text) => {
    return text.replace(/[\(\),\/]/g, "").replace(/ /g, "-").toLowerCase()
  })

}

function start() {
  try {

    const app = unconfiguredApp

    intializeExpress(app)


    logRequestStart({
      level: 'info',
      message: `Compiling internationalization files...`
    })
    const markdownFolder = 'app'
    let en = {};
    let cy = {};
    const routes = fs.readdirSync(markdownFolder).filter(f => f !== 'router.js').map(f => ({
      file: `./app/${f}/locales/`,
      name: f
    }))
    routes.forEach((file, index, array) => {
      var cylang = fs.readFileSync(`${file.file}/cy.json`).toString('utf-8');
      var enlang = fs.readFileSync(`${file.file}/en.json`).toString('utf-8');
      en = Object.assign({}, en, JSON.parse(enlang))
      cy = Object.assign({}, cy, JSON.parse(cylang))
    })

    logRequestStart({
      level: 'info',
      message: `Configuring I18n...`
    })
    i18n.configure({
      locales: ['en', 'cy'],
      defaultLocale: 'cy',
      staticCatalog: {en: en, cy: cy},
      queryParameter: "lang",
      autoReload: false,
      cookie: 'lang',
      header: 'accept-language',
      updateFiles: false,
      register: global,
      syncFiles: false,
      logDebugFn: function (msg) {
        console.log('debug', msg)
      },
      logWarnFn: function (msg) {
        console.log('warn', msg)
      },
      logErrorFn: function (msg) {
        console.log('error', msg)
      },
      missingKeyFn: function (locale, value) {
        console.log('Missing translation for language: ', locale)
        console.log('Missing translation key: ', value)
        return value
      },
    });
    i18n.setLocale('en')
    app.use(i18n.init)

    // setup nunjucks

    logRequestStart({
      level: 'info',
      message: `Setting up Nunjucks...`
    })
    setupNunjucks(app)

    // Set view engine
    app.set('view engine', 'njk')

    logRequestStart({
      level: 'info',
      message: `Adding routes...`
    })
    router.bind(app)

    app.listen(PORT)
    logRequestStart({
      level: 'info',
      message: `Server ready and listening on port ${PORT}`
    })
  }catch(e){
    logRequestStart({
      level: 'error',
      message: e.message
    })
  }
}

module.exports = {
  start
}