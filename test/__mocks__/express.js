const req = {
    query: {},
    body: {},
    cookies: {
        sessionID: '1234'
    },
    route: {
        path: '/path'
    },
    session: {},
}

const res = {
    redirect: jest.fn()
}

const next = jest.fn()

module.exports = {
    req,
    res,
    next,
}