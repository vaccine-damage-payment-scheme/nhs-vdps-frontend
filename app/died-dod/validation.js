const { check, validationResult } = require('express-validator')
const dpRepository = require('../../common/schemas/DisabledPersonDetails')
const { logRequestStart } = require('../../common/utils/request-logger')
const querystring = require('node:querystring');
const validateDateString = require("../../common/utils/validate-date-string")

// this sets the rules on the request body in the middleware
const diedDODRules = () => {
  return [

  ]
}

// while this function executes the actual rules against the request body
const diedDODValidation = () => {
  return async (req, res, next) => {

    if(req.session.errors){
        if(!req.session.errors.length){
            req.session.errors = null
        }
    }
    // execute the rules
    const errors = validationResult(req)
    const errorsInput = []

    const dodDay = req.body.dodDay
    const dodMonth = req.body.dodMonth
    const dodYear = req.body.dodYear

    if (dodDay === '' && dodMonth === '' && dodYear === '') {
      errors.errors.push({
        msg: 'common.errors.E0039',
        param: 'dodDay',
        formgroup: 'dodDate'
      })
    }

    if (errors.errors.length === 0) {
      if (dodDay === '' || (dodDay < 1 && dodDay > 31))  {
        errors.errors.push({
        msg: 'common.errors.E0040',
          param: 'dodDay',
          formgroup: 'dodDate'
        })
      }
      if (dodMonth === '' || (dodMonth < 1 && dodMonth > 12)) {
        errors.errors.push({
        msg: 'common.errors.E0041',
          param: 'dodMonth',
          formgroup: 'dodDate'
        })
      }
      if (dodYear === '') {
        errors.errors.push({
        msg: 'common.errors.E0042',
          param: 'dodYear',
          formgroup: 'dodDate'
        })

      }
      if (dodYear !== '' && dodYear.length != 4) {
        errors.errors.push({
        msg: 'common.errors.E0043',
          param: 'dodYear',
          formgroup: 'dodDate'
        })
      }
    }

    if (errors.errors.length === 0) {
      if (!validateDateString(dodDay, dodMonth, dodYear)) {
        errors.errors.push({
        msg: 'common.errors.E0022',
          param: 'dodDay',
          formgroup: 'dodDate'
        })
      }
    }

    if (errors.errors.length === 0) {
      if (isDateInTheFuture(dodDay, dodMonth, dodYear)) {
        errors.errors.push({
        msg: 'common.errors.E0044',
          param: 'dodDay',
          formgroup: 'dodDate'
        })
      }
    }

    if (errors.errors.length === 0) {
        const dodBeforeDob = await isDODAfterDOB(req, dodDay, dodMonth, dodYear)
        if (!dodBeforeDob) {
            errors.errors.push({
        msg: 'common.errors.E0045',
                param: 'dodDay',
                formgroup: 'dodDate'
            })
        }
    }


    if(req.session.errors){
        req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
            req.session.errors = [...req.session.errors, ...errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))]
        }else{
          req.session.errors = errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))
    }
    // check if any of the request body failed the requirements set in validation rules
    if (errors.isEmpty()) {
        if(req.session.errors){
            req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
        }
        if(!req.session.errors){
            if(!req.session.errors.length){
                req.session.errors = null
            }
        }
    }
    // move to next middleware
    next()
  }
}

function isDateInTheFuture(day, month, year){
    const date = new Date(year + '-' + month + '-' + day)
    const today = new Date()

    return date > today
}

async function isDODAfterDOB(req, day, month, year){
    const repo = (await dpRepository()).repository
    const dpData = await repo.fetch(req.cookies.sessionID)
    const dob = new Date(dpData.DpDateOfBirth)
    const dod = new Date(year + '-' + month + '-' + day)
   return  dod >= dob
}
module.exports = {
  diedDODValidation,
  diedDODRules
}
