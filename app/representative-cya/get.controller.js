'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const { findFile, markdown } = require('../../common/utils/markdown')
const config = require('../../common/config')
const metaRepository = require('../../common/schemas/MetaData')
const dataRepository = require('../../common/schemas/Data')
const cdRepository = require('../../common/schemas/ClaimantDetails')
const applicationStatusRepository = require('../../common/schemas/ApplicationStatus')
const claimRepository = require('../../common/schemas/Claim')
const repRepository = require('../../common/schemas/Representative')
const htmlentities = require('../../common/assets/htmlentities.json')
module.exports = () => {
  return {
    render: async (req, res) => {
              req.session.section = 'NominatedPersonDetails'
  try {
        const repo = (await dataRepository()).repository
        const data = await repo.fetch(req.cookies.sessionID)

        if(!data.AccountVerified || !data.WhyMakingClaim ){
            res.redirect('/')
            return
        }

        const metaRepo = (await metaRepository()).repository
        const metadata = await metaRepo.fetch(req.cookies.sessionID)

        const cdRepo = (await cdRepository()).repository
        const cd = await cdRepo.fetch(req.cookies.sessionID)

        const claimRepo = (await claimRepository()).repository
        const claim = await claimRepo.fetch(req.cookies.sessionID)

        const repRepo = (await repRepository()).repository
        const representative = await repRepo.fetch(req.cookies.sessionID)
        

        let displayedAddress = representative.RepresentativeAddressLine1 + ', ' + representative.RepresentativeAddressLine2 + ', ' +  representative.RepresentativeTownOrCity + ', ' +  representative.RepresentativeCounty + ', ' + representative.RepresentativePostcode
        displayedAddress = displayedAddress.replace('null, ','')
        displayedAddress = displayedAddress.replace(', ,',', ')
        representative.DisplayedAddress = displayedAddress


        if(representative.RepresentativeMobileTelNo && representative.RepresentativeHomeTelNo && representative.RepresentativeWorkTelNo){
            representative.DisplayedContactInformation = representative.RepresentativeMobileTelNo + ', ' + representative.RepresentativeHomeTelNo + ', ' + representative.RepresentativeWorkTelNo
        } else if(representative.RepresentativeMobileTelNo && representative.RepresentativeHomeTelNo){
            representative.DisplayedContactInformation = representative.RepresentativeMobileTelNo + ', ' + representative.RepresentativeHomeTelNo
        } else if(representative.RepresentativeMobileTelNo && representative.RepresentativeWorkTelNo){
            representative.DisplayedContactInformation = representative.RepresentativeMobileTelNo + ', ' + representative.RepresentativeWorkTelNo
        } else if(representative.RepresentativeHomeTelNo && representative.RepresentativeWorkTelNo){
            representative.DisplayedContactInformation = representative.RepresentativeHomeTelNo + ', ' + representative.RepresentativeWorkTelNo
        } else if(representative.RepresentativeMobileTelNo ){
            representative.DisplayedContactInformation = representative.RepresentativeMobileTelNo
        } else if(representative.RepresentativeHomeTelNo){
            representative.DisplayedContactInformation = representative.RepresentativeHomeTelNo
        } else if(representative.RepresentativeWorkTelNo){
            representative.DisplayedContactInformation = representative.RepresentativeWorkTelNo
        }
        const applicationStatusRepo = (await applicationStatusRepository()).repository
        const applicationStatus = await applicationStatusRepo.fetch(req.cookies.sessionID)
        applicationStatus.NominatedPersonDetails = 'Completed'
        if(data.ApplyingFor === 'Myself') {
            if(data.WhyMakingClaim === 'mother' || data.WhyMakingClaim === 'polio'){
               if(applicationStatus.VaccinatedPersonDetails !== 'Completed'){
                    applicationStatus.VaccinatedPersonDetails = 'ToDo'
                }
            } else {
                if(applicationStatus.VaccinesReceived !== 'Completed'){
                    applicationStatus.VaccinesReceived = 'ToDo'
                }
            }
        } else {
            if(data.ApplyingFor === 'Under16' || data.ApplyingFor === 'Disabled'  || data.ApplyingFor === 'Deceased' ){
                 if(applicationStatus.DisabledPersonDetails !== 'Completed'){
                    applicationStatus.DisabledPersonDetails = 'ToDo'
                } else if(data.WhyMakingClaim === 'mother') {
                    if(applicationStatus.VaccinatedPersonDetails !== 'Completed'){
                        applicationStatus.VaccinatedPersonDetails = 'ToDo'
                    }
                } else if(data.WhyMakingClaim === 'polio'){
                    if(applicationStatus.VaccinatedPersonDetails !== 'Completed'){
                        applicationStatus.VaccinatedPersonDetails = 'ToDo'
                    }
                }
            }
        }
        await applicationStatusRepo.save(applicationStatus)


        const templateBody = findFile('representative-cya')
        let pageTitle;
        let md = templateBody.toString();
        if(md.includes('{#<pagetitle>')){
            pageTitle = md.split('{#<pagetitle>')[1].split('</pagetitle>')[0]
            md = md.split('</pagetitle>#}')[1]
        }
        let errors = null
        if(req.session.errors)
        {
            if(req.session.errors.filter(e => e.route === req.route.path).length){
                errors = req.session.errors.filter(e => e.route === req.route.path)
            }
        }
        

        const markdownRender = markdown(templateBody.toString())
        const params = {
          content: markdownRender,
          file: `./app/representative-cya/templates/index.njk`,
          errors: errors || null,
          data: {
            ClaimantDetails: cd,
            MetaData: metadata,
            Data: data,
            Claim: claim,
            Representative: representative
          },
          title: pageTitle ? pageTitle: 'PLEASE SET A PAGE TITLE'
        }
        res.render('app/index/index', params)
      } catch (e) {
        logRequestStart({
          level: 'error',
          message: e.message
        })

        return res.redirect('back')
      }
    }
  }
}
