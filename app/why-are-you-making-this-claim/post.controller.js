'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const Data = require('../../common/schemas/Data')
const dataRepository = require('../../common/schemas/Data')
module.exports = () => {
  return {
    render: async (req, res) => {
        if(!req.body){
            res.redirect('why-are-you-making-this-claim')
        }
      try {
        const repo = (await dataRepository()).repository
        const data = await repo.fetch(req.cookies.sessionID)
        data.WhyMakingClaim = req.body.WhyMakingClaim
        await repo.save(data)
        if (req.body.WhyMakingClaim === 'receivedVaccine' || req.body.WhyMakingClaim === 'mother' || req.body.WhyMakingClaim === 'polio') {
          res.redirect('about-you-name')
        } else {
          res.redirect('error-page')
        }
      } catch (e) {
        logRequestStart({
          level: 'error',
          message: e.message
        })
        return res.redirect('why-are-you-making-this-claim')
      }
    }
  }
}
