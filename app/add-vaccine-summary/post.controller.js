'use strict'
const dataRepository = require('../../common/schemas/Data')
const config = require('../../common/config')
const { findFile, markdown } = require('../../common/utils/markdown')
const applicationStatusRepository = require('../../common/schemas/ApplicationStatus')

module.exports = () => {
    return {
        render: async (req, res) => {
                req.session.section = 'VaccinesReceived'
  const dataRepo = (await dataRepository()).repository
            const data = await dataRepo.fetch(req.cookies.sessionID)
            if (req.body.addAnotherVaccine === 'Yes') {
                if (data.ApplyingFor === "Myself"){
                    res.redirect('add-vaccine')
                } else if (data.ApplyingFor === "Deceased"){
                    res.redirect('add-vaccine-died')
                } else {
                    res.redirect('add-vaccine-other')
                }

            } else {

                const applicationStatusRepo = (await applicationStatusRepository()).repository
                const applicationStatus = await applicationStatusRepo.fetch(req.cookies.sessionID)
                applicationStatus.VaccinesReceived = 'Completed'
                applicationStatus.DatesOfVaccination = 'Completed'
                if(applicationStatus.WhatHappenedNext !== 'Completed'){
                    applicationStatus.WhatHappenedNext = 'ToDo'
                }
                await applicationStatusRepo.save(applicationStatus)

                res.redirect('vaccine-diagnosis')
            }

        }
    }
}
