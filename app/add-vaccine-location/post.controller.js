'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const vaccineRepository = require('../../common/schemas/Vaccinations')
const metaRepository = require('../../common/schemas/MetaData')
const dataRepository = require('../../common/schemas/Data')

module.exports = () => {
  return {
      render: async (req, res) => {
            req.session.section = 'VaccinesReceived'
  try {
            const metaRepo = (await metaRepository()).repository
            const metaData = await metaRepo.fetch(req.cookies.sessionID)

            const dataRepo = (await dataRepository()).repository
            const data = await dataRepo.fetch(req.cookies.sessionID)

            const count = metaData.NumberOfVaccines;
            const vcRepo = (await vaccineRepository()).repository
            const vaccine = await vcRepo.fetch(req.cookies.sessionID + ':' + count)
            vaccine.VaccinationCountry = req.body.vaccineLocation
            await vcRepo.save(vaccine)

            if(req.session.errors){
                if(req.session.errors.find(e => e.route === req.route.path)){
                  res.redirect('add-vaccine-location')
                  return
                }
                if(!req.session.errors.length){
                    req.session.errors = null
                };
            }else{
                req.session.errors = null
            }

            if(req.body.vaccineLocation === 'United Kingdom'){
                res.redirect('add-vaccine-country')
            } else {
                if(data.ApplyingFor === 'Myself')
                    res.redirect('armed-forces-check')
                else
                    res.redirect('armed-forces-check-died')
            }

        } catch (e) {
          logRequestStart({
            level: 'error',
            message: e.message
          })
          return res.redirect('add-vaccine')
        }
      }
    }
}
