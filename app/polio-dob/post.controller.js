'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const vpRepository = require('../../common/schemas/VaccinatedPersonDetails')
const dataRepository = require('../../common/schemas/Data')
const config = require('../../common/config')
const { findFile, markdown } = require('../../common/utils/markdown')
const querystring = require('node:querystring');
const { formatDateString } = require('../../common/utils/string');

module.exports = () => {
    return {
        render: async (req, res) => {
            try {
                req.session.section = 'VaccinesReceived'
                let dobDay = req.body.dobDay
                let dobMonth = req.body.dobMonth
                const dobYear = req.body.dobYear

                 if(req.session.errors){
                    req.session.body = req.body
                    res.redirect('polio-dob')
                    return
                  } else {
                    const repo = (await vpRepository()).repository
                    const vpData = await repo.fetch(req.cookies.sessionID)
                    vpData.VpDateOfBirth = formatDateString(dobYear, dobMonth, dobDay)
                    await repo.save(vpData)
                  }




            } catch (e) {
                logRequestStart({
                    level: 'error',
                    message: e.message
                })
            }
            if(req.session.errors){
                res.redirect('polio-dob')
                return
            }

            req.session.errors = null
            const dataRepo = (await dataRepository()).repository
            const data = await dataRepo.fetch(req.cookies.sessionID)
            res.redirect('polio-cya')
            return
        }
    }
}


function validateDateString (date) {

    if(date){
        const splitDate = date.split('-');
        if(!splitDate[0] || !splitDate[1] || !splitDate[2]){
            return false;
        } else {
            return true;
        }
   }
   return false;
}