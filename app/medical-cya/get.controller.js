'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const { findFile, markdown } = require('../../common/utils/markdown')
const config = require('../../common/config')
const metaRepository = require('../../common/schemas/MetaData')
const dataRepository = require('../../common/schemas/Data')
const gpRepository = require('../../common/schemas/GeneralPractice')
const hospitalRepository = require('../../common/schemas/Hospitals')
const healthcareProviderRepository = require('../../common/schemas/HealthcareProviders')
const applicationStatusRepository = require('../../common/schemas/ApplicationStatus')
const claimRepository = require('../../common/schemas/Claim')
const htmlentities = require('../../common/assets/htmlentities.json')

module.exports = () => {
  return {
    render: async (req, res) => {
      try {
        let Hospitals = []
        let hospital
        let hospitalDisplayedAddress

        let HealthcareProviders = []
        let healthCareProvider
        let healthCareProviderDisplayedAddress
        let gplink

        const gpRepo = (await gpRepository()).repository
        const gpData = await gpRepo.fetch(req.cookies.sessionID)

        const dataRepo = (await dataRepository()).repository
        const data = await dataRepo.fetch(req.cookies.sessionID)

        if(!data.AccountVerified || !data.WhyMakingClaim ){
            res.redirect('/')
            return
        }

        if (data.ApplyingFor === "Under16" || data.ApplyingFor === "Disabled")
            gplink="/gp-details-other"
        else if (data.ApplyingFor === "Myself")
            gplink="/gp-details"
        else
            gplink="/gp-details-died"



        const hospitalRepo = (await hospitalRepository()).repository

        const hcpRepo = (await healthcareProviderRepository()).repository

        const metaRepo = (await metaRepository()).repository
        const metadata = await metaRepo.fetch(req.cookies.sessionID)

        // GP Details
        let gpDisplayedAddress =  gpData.GpName + ', ' + gpData.GpAddressLine1 + ', ' + gpData.GpAddressLine2 + ', ' +  gpData.GpTownOrCity + ', ' +  gpData.GpCounty + ', ' + gpData.GpPostcode
        gpDisplayedAddress = gpDisplayedAddress.replace('null,','')
        gpDisplayedAddress = gpDisplayedAddress.replace('null,','')
        gpDisplayedAddress = gpDisplayedAddress.replace('null,','')
        gpDisplayedAddress = gpDisplayedAddress.replace('null,','')
        gpDisplayedAddress = gpDisplayedAddress.replace(', ,',', ')
        gpDisplayedAddress = gpDisplayedAddress.replace(', ,',', ')
        gpDisplayedAddress = gpDisplayedAddress.replace(', ,',', ')
        gpDisplayedAddress = gpDisplayedAddress.replace(', ,',', ')
        gpData.DisplayedAddress = gpDisplayedAddress


        // Hospital Details
        const hospitalCount = metadata.NumberOfHospitals

        for(let i=1; i<=hospitalCount; i++){
            hospital = await hospitalRepo.fetch(req.cookies.sessionID+":"+i)
            if(hospital.DisplayOnline){
                hospitalDisplayedAddress = hospital.HospitalAddressLine1 + ', ' + hospital.HospitalAddressLine2 + ', ' +  hospital.HospitalTownOrCity + ', ' +  hospital.HospitalCounty + ', ' + hospital.HospitalPostcode
                hospitalDisplayedAddress = hospitalDisplayedAddress.replace('null,','')
                hospitalDisplayedAddress = hospitalDisplayedAddress.replace('null,','')
                hospitalDisplayedAddress = hospitalDisplayedAddress.replace('null,','')
                hospitalDisplayedAddress = hospitalDisplayedAddress.replace('null,','')
                hospitalDisplayedAddress = hospitalDisplayedAddress.replace(', ,',', ')
                hospital.DisplayedAddress = hospitalDisplayedAddress
                Hospitals.push(hospital)
            }
        }

        // Healthcare Provider Details
        const healthcareProvidersCount = metadata.NumberOfHealthcareProviders

        for(let i=1; i<=healthcareProvidersCount; i++){
            healthCareProvider = await hcpRepo.fetch(req.cookies.sessionID+":"+i)
            if (healthCareProvider.DisplayOnline){
                healthCareProviderDisplayedAddress = healthCareProvider.ClinicAddressLine1 + ', ' + healthCareProvider.ClinicAddressLine2 + ', ' +  healthCareProvider.ClinicTownOrCity + ', ' +  healthCareProvider.ClinicCounty + ', ' + healthCareProvider.ClinicPostcode
                healthCareProviderDisplayedAddress = healthCareProviderDisplayedAddress.replace('null,','')
                healthCareProviderDisplayedAddress = healthCareProviderDisplayedAddress.replace('null,','')
                healthCareProviderDisplayedAddress = healthCareProviderDisplayedAddress.replace('null,','')
                healthCareProviderDisplayedAddress = healthCareProviderDisplayedAddress.replace('null,','')
                healthCareProviderDisplayedAddress = healthCareProviderDisplayedAddress.replace(', ,',', ')
                healthCareProviderDisplayedAddress = healthCareProviderDisplayedAddress.replace(', ,',', ')
                healthCareProviderDisplayedAddress = healthCareProviderDisplayedAddress.replace(', ,',', ')
                healthCareProviderDisplayedAddress = healthCareProviderDisplayedAddress.replace(', ,',', ')
                healthCareProvider.DisplayedAddress = healthCareProviderDisplayedAddress
                HealthcareProviders.push(healthCareProvider)
            }
        }



        const templateBody = findFile('medical-cya')
        let pageTitle;
        let md = templateBody.toString();
        if(md.includes('{#<pagetitle>')){
            pageTitle = md.split('{#<pagetitle>')[1].split('</pagetitle>')[0]
            md = md.split('</pagetitle>#}')[1]
        }
        let errors = null
        if(req.session.errors)
        {
            if(req.session.errors.filter(e => e.route === req.route.path).length){
                errors = req.session.errors.filter(e => e.route === req.route.path)
            }
        }
        const markdownRender = markdown(templateBody.toString())

        const params = {
          content: markdownRender,
          file: `./app/medical-cya/templates/index.njk`,
          errors: errors || null,
          data: {
            GeneralPractice: gpData,
            GPLink: gplink,
            Hospitals: Hospitals,
            HealthcareProviders: HealthcareProviders,
            MetaData: metadata
          },
          title: pageTitle ? pageTitle: 'PLEASE SET A PAGE TITLE'
        }
        res.render('app/index/index', params)
      } catch (e) {
        logRequestStart({
          level: 'error',
          message: e.message
        })

        return res.redirect('back')
      }
    }
  }
}

