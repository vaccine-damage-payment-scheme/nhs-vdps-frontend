const { req, res } = require('express');
const postController = require('./post.controller');
const vaccineRepository = require('../../common/schemas/Vaccinations');
const metaRepository = require('../../common/schemas/MetaData');

jest.mock('../../common/schemas/MetaData');
jest.mock('../../common/schemas/Vaccinations');


describe('Post Controller', () => {
    test('should redirect to add-vaccine-details page', async () => {
        await postController.post(req, res);
        expect(res.redirect).toHaveBeenCalledTimes(1);
        expect(res.redirect.mock.calls[0][0]).toEqual('add-vaccine-details');
    });

    test('should redirect to add-vaccine-country page when req is invalid', async () => {
        await postController.post({}, res);
        expect(res.redirect).toHaveBeenCalledTimes(1);
        expect(res.redirect.mock.calls[0][0]).toEqual('add-vaccine-country');
    });

    test('should call fetch metaRepo with the correct param', async () => {
        const { repository: metaRepo } = await metaRepository();

        await postController.post(req, res);
        expect(metaRepo.fetch).toHaveBeenCalledTimes(1);
        expect(metaRepo.fetch.mock.calls[0][0]).toEqual('1234');
    });

    test('should call vaccRepo.fetch with the correct param', async () => {
        const { repository: vaccRepo } = await vaccineRepository();

        await postController.post(req, res);
        expect(vaccRepo.fetch).toHaveBeenCalledTimes(1);
        expect(vaccRepo.fetch.mock.calls[0][0]).toEqual('1234:1');
    });

    test('should call vaccRepo.fetch when vaccineCount = 3', async () => {
        const { repository: vaccRepo } = await vaccineRepository();
        req.body.vaccineCount = 3;
        await postController.post(req, res);
        expect(vaccRepo.fetch).toHaveBeenCalledTimes(1);
        expect(vaccRepo.fetch.mock.calls[0][0]).toEqual('1234:3');
    });

    test.each([
        "England",
        "Northern Ireland",
        "Scotland",
        "Wales",
        "Isle of Man",
    ])('should call vaccRepo.save when country = %s', async (country) => {
        const { repository: vaccRepo } = await vaccineRepository();
        req.body.country = country;
        await postController.post(req, res);
        expect(vaccRepo.save).toHaveBeenCalledTimes(1);
        expect(vaccRepo.save.mock.calls[0][0]).toEqual({
            VaccinationCountry: country
        });
    });
});
