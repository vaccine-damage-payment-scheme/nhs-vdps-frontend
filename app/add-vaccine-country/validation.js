const VALID_COUNTRIES = [
  "England",
  "Northern Ireland",
  "Scotland",
  "Wales",
  "Isle of Man",
];

module.exports.addVaccineCountryValidation = () => {
  return (req, res, next) => {
    const errors = [];

    if (!req.body.country || !VALID_COUNTRIES.includes(req.body.country)) {
        errors.push({
          route: req.route.path,
          msg: 'common.errors.E0016',
          param: 'country'
        })
      }

    if(errors.length > 0){
        req.session.errors = errors;
        return res.redirect(req.route.path)
    }
    delete req.session.errors

    next()
}
}