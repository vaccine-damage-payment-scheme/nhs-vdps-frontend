const { logRequestStart } = require('../../common/utils/request-logger');
const vaccineRepository = require('../../common/schemas/Vaccinations');
const metaRepository = require('../../common/schemas/MetaData');

module.exports.post = async (req, res) => {
    try {
        const { repository: metaRepo } = await metaRepository();

        const metaData = await metaRepo.fetch(req.cookies.sessionID);
        const count = req.body.vaccineCount || metaData.NumberOfVaccines || 1;

        const { repository: vcRepo } = await vaccineRepository();
        const vaccine = await vcRepo.fetch(req.cookies.sessionID + ':' + count);

        vaccine.VaccinationCountry = req.body.country;
        await vcRepo.save(vaccine);

        return res.redirect('add-vaccine-details');
    } catch (error) {
        logRequestStart({
            level: 'error',
            message: error.message
        });
        return res.redirect('add-vaccine-country');
    }
};
