const { check, validationResult } = require('express-validator')
const claimRepository = require('../../common/schemas/Claim')
const dataRepository = require('../../common/schemas/Data')
const applicationStatusRepository = require('../../common/schemas/ApplicationStatus')

// this sets the rules on the request body in the middleware
const declarationRules = () => {
    return [
        check('declarationFirstName')
            .trim()
            .isLength({ min: 2 })
            .withMessage('common.errors.E0024')
            .bail()
            .isLength({ max: 100 })
            .withMessage('common.errors.E0025')
            .bail()
            .matches(/^[A-Za-z \'\-]+$/)
            .withMessage('common.errors.E0026'),
        check('declarationLastName')
            .trim()
            .isLength({ min: 2 })
            .withMessage('common.errors.E0030')
            .bail()
            .isLength({ max: 100 })
            .withMessage('common.errors.E0031')
            .bail()
            .matches(/^[A-Za-z \'\-]+$/)
            .withMessage('common.errors.E0032'),
        check('declarationA')
            .isIn(['declarationA'])
            .withMessage('common.errors.E0037')
    ]
}

// while this function executes the actual rules against the request body
const declarationValidation = () => {

    return async (req, res, next) => {
        const dataRepo = (await dataRepository()).repository
        const data = await dataRepo.fetch(req.cookies.sessionID)
        // execute the rules
        const errors = validationResult(req)
        const applicationStatusRepo = (await applicationStatusRepository()).repository
        const applicationStatus = await applicationStatusRepo.fetch(req.cookies.sessionID)
        if(!errors.isEmpty()){
            applicationStatus.Declaration = 'ToDo'
        }
        let route = req.route.path;
        if(data.ApplyingFor === 'Deceased'){
            route = '/declaration-died'
        } else if(data.ApplyingFor === 'Under16' || data.ApplyingFor === 'Disabled'){
            route = '/declaration-other'
        } else{
            route = '/declaration'
        }


        req.session.errors = errors.errors.map(err => Object.assign({},err,{route: route}))

        if (!errors.isEmpty()) {
            req.session.body = req.body;

           // heres where you send the errors or do whatever you please with the error, in  your case
           if(data.ApplyingFor === 'Deceased'){
               res.redirect('declaration-died')
           } else if(data.ApplyingFor === 'Under16' || data.ApplyingFor === 'Disabled'){
               res.redirect('declaration-other')
           } else{
               res.redirect('declaration')
           }
           return
         }

        if (errors.isEmpty()) {
            if(req.session.errors){
                req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
            }
            if(!req.session.errors){
                if(!req.session.errors.length){
                    req.session.errors = null
                }
            }
            applicationStatus.Declaration = 'Completed'
        }

        await applicationStatusRepo.save(applicationStatus)

        next()
    }
}
module.exports = {
  declarationRules,
  declarationValidation
}
