'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const { findFile, markdown } = require('../../common/utils/markdown')
const config = require('../../common/config')
const dataRepository = require('../../common/schemas/Data')
const metaRepository = require('../../common/schemas/MetaData')
const cdRepository = require('../../common/schemas/ClaimantDetails')
const vpRepository = require('../../common/schemas/VaccinatedPersonDetails')
const dpRepository = require('../../common/schemas/DisabledPersonDetails')
const claimRepository = require('../../common/schemas/Claim')
const applicationStatusRepository = require('../../common/schemas/ApplicationStatus')
const representativeRepository = require('../../common/schemas/Representative')
const { I18n } = require('i18n')
module.exports = () => {

    return {
        render: async (req, res) => {
            try {
                const repo = (await metaRepository()).repository
                const metaData = (await repo.fetch(req.cookies.sessionID)).toJSON()

                const cdRepo = (await cdRepository()).repository
                const cd = (await cdRepo.fetch(req.cookies.sessionID)).toJSON()

                const vpRepo = (await vpRepository()).repository
                const vp = (await vpRepo.fetch(req.cookies.sessionID)).toJSON()

                const dpRepo = (await dpRepository()).repository
                const dp = (await dpRepo.fetch(req.cookies.sessionID)).toJSON()

                const claimRepo = (await claimRepository()).repository
                const claim = (await claimRepo.fetch(req.cookies.sessionID)).toJSON()

                const applicationStatusRepo = (await applicationStatusRepository()).repository
                const applicationStatus = (await applicationStatusRepo.fetch(req.cookies.sessionID)).toJSON()
                const applicationStatusU = await applicationStatusRepo.fetch(req.cookies.sessionID)

                const representativeRepo = (await representativeRepository()).repository
                const representative = (await representativeRepo.fetch(req.cookies.sessionID)).toJSON()

                const dataRepo = (await dataRepository()).repository
                const data = (await dataRepo.fetch(req.cookies.sessionID)).toJSON()
                /*if (!data.Email) {
                  req.session.errors = [{param: 'na', msg:'Account Not Verified.'}]
                  return res.redirect('/')
                }*/

                let yourDetailsLink = '/about-you-cya'

                let vaccineLink = 'add-vaccine'
                let vaccineDatesLink = 'add-vaccine'

                let representativeLink = '/representative-question'

                let doctorLink = 'gp-details'
                let hospitalLink = 'visited-hospital'
                let healthClinicLink = 'visited-healthcare-provider'
                let disabledPersonDetailsLink = 'died-name'
                let declarationLink = 'declaration'
                let polioNameLink = 'polio-name'

                if(applicationStatus.YourDetails === 'Completed') {
                    if (data.ApplyingFor === "Myself")
                        yourDetailsLink = '/about-you-cya'
                    else
                        yourDetailsLink = '/other-about-you-cya'
                }else{

                }
                // check if representative is completed
                if (applicationStatus.NominatedPersonDetails === 'Completed') {
                  if (representative) {
                    if (representative.RepresentativePostcode) {
                      representativeLink = '/representative-cya'
                    }
                  } else {
                    representativeLink = 'representative-question'
                  }
                } else if (applicationStatus.NominatedPersonDetails === 'Cannot-start-yet') {
                    representativeLink = 'your-application'
                }
                if (applicationStatus.VaccinesReceived === 'Completed') {
                    vaccineLink = 'vaccine-cya'
                    vaccineDatesLink = 'vaccine-cya'
                } else if (applicationStatus.VaccinesReceived === 'Cannot-start-yet') {
                    vaccineLink = 'your-application'
                    vaccineDatesLink = 'your-application'
                } else {
                    vaccineLink = 'medical-history-info'
                    vaccineDatesLink = 'medical-history-info'
                }
                // check if doctor has been completed
                if(data.ApplyingFor === 'Deceased'){
                    doctorLink = 'gp-details-died'
                }
                if(data.ApplyingFor === 'Under16' || data.ApplyingFor === 'Disabled'){
                    doctorLink = 'gp-details-other'
                }
                if (applicationStatus.DoctorDetails === 'Completed') {
                    doctorLink = 'hospital-cya'
                } else if (applicationStatus.DoctorDetails === 'Cannot-start-yet') {
                    doctorLink = 'your-application'
                }

                // check if hospital has been completed
                if(data.ApplyingFor === 'Deceased'){
                    hospitalLink = 'hospital-details-died'
                }
                // check if hospital has been completed
                if(data.ApplyingFor === 'Under16' || data.ApplyingFor === 'Disabled'){
                    hospitalLink = 'hospital-details-other'
                }
                if (applicationStatus.HospitalAttendance === 'Completed') {
                  hospitalLink = 'hospital-cya'
                } else if (applicationStatus.HospitalAttendance === 'Cannot-start-yet') {
                    hospitalLink = 'your-application'
                }

                // check if health clinic has been completed
                if (applicationStatus.HealthClinicAttendance === 'Completed') {
                    healthClinicLink = 'healthcare-provider-cya'
                } else if (applicationStatus.HealthClinicAttendance === 'Cannot-start-yet') {
                    healthClinicLink = 'your-application'
                }

                if (applicationStatus.Declaration === 'Cannot-start-yet') {
                    declarationLink = 'your-application'
                } else {
                    if(data.ApplyingFor === 'Deceased'){
                        declarationLink = 'declaration-died'
                    } else
                    if(data.ApplyingFor === 'Under16' || data.ApplyingFor === 'Disabled'){
                        declarationLink = 'declaration-other'
                    } else {
                        declarationLink = 'declaration'
                    }
                }

                if (applicationStatus.DisabledPersonDetails === 'Completed') {
                    if(data.ApplyingFor === 'Deceased'){
                        disabledPersonDetailsLink = 'died-cya'
                    }
                    if(data.ApplyingFor === 'Under16' || data.ApplyingFor === 'Disabled'){
                        disabledPersonDetailsLink = 'other-cya'
                    }
                } else if (applicationStatus.DisabledPersonDetails === 'Cannot-start-yet') {
                    disabledPersonDetailsLink = 'your-application'
                }

                let steps
                let died = false
                let other = false
                let umoa = false
                let mother = false
                let polio = false

                if(data.ApplyingFor === 'Deceased'){
                    if (applicationStatus.DisabledPersonDetails === 'Cannot-start-yet') {
                        disabledPersonDetailsLink = 'your-application'
                    } else if (applicationStatus.DisabledPersonDetails === 'ToDo') {
                        disabledPersonDetailsLink = 'died-name'
                    } else {
                        disabledPersonDetailsLink = 'died-cya'
                    }
                    died = true;
                }

                if(data.ApplyingFor === 'Under16' || data.ApplyingFor === 'Disabled' ){
                    if (applicationStatus.DisabledPersonDetails === 'Cannot-start-yet') {
                        disabledPersonDetailsLink = 'your-application'
                    } else if (applicationStatus.DisabledPersonDetails === 'ToDo') {
                        disabledPersonDetailsLink = 'other-name'
                    } else {
                        disabledPersonDetailsLink = 'other-cya'
                    }
                    umoa = true
                }

                let vaccinatedPersonDetailsLink

                if(data.WhyMakingClaim === 'mother' ){
                    if (applicationStatus.VaccinatedPersonDetails === 'Cannot-start-yet') {
                        vaccinatedPersonDetailsLink = 'your-application'
                    } else if (applicationStatus.VaccinatedPersonDetails === 'ToDo' && data.ApplyingFor === 'Myself') {
                        vaccinatedPersonDetailsLink = 'mothers-name'
                    } else if (applicationStatus.VaccinatedPersonDetails === 'ToDo' && data.ApplyingFor !== 'Myself') {
                        vaccinatedPersonDetailsLink = 'other-mothers-name'
                    } else if (applicationStatus.VaccinatedPersonDetails === 'Completed' && data.ApplyingFor === 'Myself') {
                        vaccinatedPersonDetailsLink = 'mothers-cya'
                    } else if (applicationStatus.VaccinatedPersonDetails === 'Completed' && data.ApplyingFor !== 'Myself') {
                        vaccinatedPersonDetailsLink = 'other-mothers-cya'
                    }
                    mother = true
                }

                if(data.WhyMakingClaim === 'polio' ){
                    if (applicationStatus.VaccinatedPersonDetails === 'Cannot-start-yet') {
                        polioNameLink = 'your-application'
                    } else if (applicationStatus.VaccinatedPersonDetails === 'ToDo') {
                        polioNameLink = 'polio-name'
                    } else if (applicationStatus.VaccinatedPersonDetails === 'Completed') {
                        polioNameLink = 'polio-cya'
                    }

                    if (applicationStatus.DatesOfVaccination === 'Completed') {
                        vaccineLink = 'vaccine-cya'
                        vaccineDatesLink = 'vaccine-cya'
                    } else if (applicationStatus.DatesOfVaccination === 'Cannot-start-yet') {
                        vaccineLink = 'your-application'
                        vaccineDatesLink = 'your-application'
                    } else {
                        vaccineLink = 'polio-dates'
                        vaccineDatesLink = 'polio-dates'
                    }

                    polio = true
                }


                steps = await getMySelfSteps(applicationStatus, yourDetailsLink, representativeLink, vaccineLink, doctorLink, hospitalLink, healthClinicLink, declarationLink, disabledPersonDetailsLink, vaccinatedPersonDetailsLink, polioNameLink, died, umoa, mother, polio, applicationStatusU)

                let nextStep = steps.map(s => s.children.find(c => c.status === 'ToDo')).find(s => s ? s.status === 'ToDo' : null)


                //if(req.session?.errors)
                //    if(req.route.path !== req.session.errors[0]?.route)
                //        if(!req.session.errors.length){


                const templateBody = findFile('your-application')
                let pageTitle;
                let md = templateBody.toString();
                if(md.includes('{#<pagetitle>')){
                    pageTitle = md.split('{#<pagetitle>')[1].split('</pagetitle>')[0]
                    md = md.split('</pagetitle>#}')[1]
                }


        let errors = null
        if(req.session.errors)
        {
            if(req.session.errors.filter(e => e.route === req.route.path).length){
                errors = req.session.errors.filter(e => e.route === req.route.path)
            }
        }
                const markdownRender = markdown(templateBody.toString())
                const params = {
                    content: markdownRender,
                    file: `./app/your-application/templates/index.njk`,
                    errors: errors || null,
                    data: {
                        ClaimantDetails: cd,
                        Data: data,
                        steps: steps,
                        nextStep: nextStep
                    },
                    title: pageTitle ? pageTitle: 'PLEASE SET A PAGE TITLE'
                }
                res.render('app/your-application/index', params)
            } catch (e) {
                logRequestStart({
                    level: 'error',
                    message: e.message
                })
                //        return res.redirect('back')
            }
        }
    }
}


async function getMySelfSteps(applicationStatus, yourDetailsLink, representativeLink, vaccineLink, doctorLink, hospitalLink, healthClinicLink, declarationLink, disabledPersonDetailsLink, vaccinatedPersonDetailsLink, polioNameLink, died = false, umoa = false, mother = false, polio = false, applicationStatusU){
    const applicationStatusRepo2 = (await applicationStatusRepository()).repository
    const steps = [{
        id:"your-application.aboutYou",
        children: [
            {
              id:'your-application.yourDetails',
              link: yourDetailsLink,
              status: (applicationStatus.YourDetails === 'undefined' || !applicationStatus.YourDetails) ? 'ToDo' : applicationStatus.YourDetails
            }
          ]
        },
        {
          id:"your-application.nominatedPerson",
          body: 'your-application.nominatedPersonBody',
          children: [
            {
              id:'your-application.nominatedPersonDetails',
              link: representativeLink,
              status: applicationStatus.NominatedPersonDetails
            }
          ]
        }]
        if(died){
            steps.push({
                  id:'your-application.aboutDied',
                  children: [
                      {
                          id: 'your-application.theirDetails',
                          link: disabledPersonDetailsLink,
                          status: (applicationStatus.DisabledPersonDetails === 'undefined' || !applicationStatus.DisabledPersonDetails) ? 'ToDo' : applicationStatus.DisabledPersonDetails
                      }
                ]
              })
        }else{
            if(!mother && !polio){
               if(applicationStatus.VaccinatedPersonDetails === 'ToDo'){
                    applicationStatusU.VaccinatedPersonDetails = 'Completed'
                }
                await applicationStatusRepo2.save(applicationStatusU)
            }
        }
        if(umoa){
            steps.push({
                  id:'your-application.aboutDisabled',
                  children: [
                      {
                          id: 'your-application.theirDetails',
                          link: disabledPersonDetailsLink,
                          status: (applicationStatus.DisabledPersonDetails === 'undefined' || !applicationStatus.DisabledPersonDetails) ? 'ToDo' : applicationStatus.DisabledPersonDetails
                      }
                ]
              })
        }else{
            if(applicationStatus.DisabledPersonDetails === 'ToDo'){
                applicationStatusU.DisabledPersonDetails = 'Completed'
            }
        }
        if(mother){
            steps.push({
                id:'your-application.aboutMother',
                children: [
                    {
                        id: 'your-application.theirDetails',
                        link: vaccinatedPersonDetailsLink,
                        status: (applicationStatus.VaccinatedPersonDetails === 'undefined' || !applicationStatus.VaccinatedPersonDetails) ? 'ToDo' : applicationStatus.VaccinatedPersonDetails
                    }
              ]
            })
        }
        if(!polio){
           steps.push({
                id:"your-application.vaccinations",
                children: [
                    {
                        id:"your-application.vaccinesReceived",
                        link: vaccineLink,
                        status: applicationStatus.VaccinesReceived
                    },
                    {
                        id:"your-application.datesOfVaccination",
                        link: vaccineLink,
                        status: applicationStatus.DatesOfVaccination
                    },
                    {
                        id:"your-application.whatHappenedNext",
                        link: vaccineLink,
                        status: applicationStatus.WhatHappenedNext
                    }
                ]
            })
        } else {
            steps.push({
                id:'your-application.aboutPolioVaccinatedPerson',
                children: [
                    {
                        id: 'your-application.theirDetails',
                        link: polioNameLink,
                        status: (applicationStatus.VaccinatedPersonDetails === 'undefined' || !applicationStatus.VaccinatedPersonDetails) ? 'ToDo' : applicationStatus.VaccinatedPersonDetails
                    },
                    {
                        id:"your-application.datesOfVaccination",
                        link: vaccineLink,
                        status: applicationStatus.DatesOfVaccination
                    },
                    {
                        id:"your-application.whatHappenedNext",
                        link: vaccineLink,
                        status: applicationStatus.WhatHappenedNext
                    }
              ]
            })
        }

    return [...steps, ...[{
                     id:"your-application.medicalInformation",
                     children: [
                       {
                         id:"your-application.gpDetails",
                         link: doctorLink,
                         status: (applicationStatus.DoctorDetails === 'undefined') ? '' : applicationStatus.DoctorDetails
                       },
                       {
                         id:"your-application.hospitalAttendance",
                         link: hospitalLink,
                         status: (applicationStatus.HospitalAttendance === 'undefined') ? '' : applicationStatus.HospitalAttendance
                       },
                       {
                         id:"your-application.hcpAttendance",
                         link: healthClinicLink,
                         status: (applicationStatus.HealthClinicAttendance === 'undefined') ? '' : applicationStatus.HealthClinicAttendance
                       }
                     ]
                   },
                   {
                     id:"your-application.submitApplication",
                     children: [
                       {
                         id:"your-application.declaration",
                         link: declarationLink,
                         status: applicationStatus.Declaration
                       }
                     ]
                   }]]
}