'use strict'
const dataRepository = require('../../common/schemas/Data')
const repRepository = require('../../common/schemas/Representative')
const { logRequestStart } = require('../../common/utils/request-logger')
const config = require('../../common/config')
const { findFile, markdown } = require('../../common/utils/markdown')
;
module.exports = () => {
  return {
    render: async (req, res) => {
          req.session.section = 'NominatedPersonDetails'

        try {
          const representativeRepo = (await repRepository()).repository
          const representative = await representativeRepo.fetch(req.cookies.sessionID)

          representative.RepresentativeAddressLine1 = req.body.RepresentativeAddressLine1
          representative.RepresentativeAddressLine2 = req.body.RepresentativeAddressLine2
          representative.RepresentativeTownOrCity = req.body.RepresentativeTownOrCity
          representative.RepresentativeCounty = req.body.RepresentativeCounty ? req.body.RepresentativeCounty : null
          representative.RepresentativePostcode = req.body.RepresentativePostcode
          await representativeRepo.save(representative)
           if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
              res.redirect('representative-address')
              return
            }
            if(req.session.errors){
                if(!req.session.errors.length){
                    req.session.errors = null
                }
            }
            res.redirect('representative-contact')
        } catch (e) {
          logRequestStart({
            level: 'error',
            message: e.message
          })

        }
    }
  }
}
