const { check, validationResult } = require('express-validator')
const cdRepository = require('../../common/schemas/ClaimantDetails')
const { logRequestStart } = require('../../common/utils/request-logger')
const applicationStatusRepository = require('../../common/schemas/ApplicationStatus')

// this sets the rules on the request body in the middleware
const aboutYouDiedRelationshipRules = () => {
  return [
 check('CdRelationship')
        .isLength({ min: 2 })
        .withMessage('common.errors.E0016')
        .bail()
        .escape()
  ]
}

// while this function executes the actual rules against the request body
const aboutYouDiedRelationshipValidation = () => {
    return async (req, res, next) => {
        const errors = validationResult(req)
        if(!errors.isEmpty()){
            errors.errors = errors.errors.map(e => Object.assign({},e,{ param:'Husband', route: req.route.path}))
        }
        if(req.session.errors){
        req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
            req.session.errors = [...req.session.errors, ...errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))]
        }else{
          req.session.errors = errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))
        }
        if(!errors.isEmpty()){
            const applicationStatusRepo = (await applicationStatusRepository()).repository
            const applicationStatus = await applicationStatusRepo.fetch(req.cookies.sessionID)

            applicationStatus.YourDetails = 'ToDo'

            await applicationStatusRepo.save(applicationStatus)
        }

        if (errors.isEmpty()) {
            if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
                req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
            }
            if(req.session.errors){
                if(!req.session.errors.length){
                    req.session.errors = null
                }
            }
        }

        next()
    }
}
module.exports = {
  aboutYouDiedRelationshipValidation,
  aboutYouDiedRelationshipRules
}
