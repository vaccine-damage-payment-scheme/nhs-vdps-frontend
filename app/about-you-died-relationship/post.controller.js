'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const cdRepository = require('../../common/schemas/ClaimantDetails')
const config = require('../../common/config')
const { findFile, markdown } = require('../../common/utils/markdown')


module.exports = () => {
    return {
        render: async (req, res) => {
             req.session.section = 'YourDetails'
     if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
                res.redirect('about-you-died-relationship')
                return
            }

            const cdRepo = (await cdRepository()).repository
            const cd = await cdRepo.fetch(req.cookies.sessionID)
            cd.CdRelationship = req.body.CdRelationship ? req.body.CdRelationship : null
            await cdRepo.save(cd)

            if(req.session.errors){
                if(!req.session.errors.length){
                    req.session.errors = null
                }
            }
            res.redirect('about-you-dob')
            return
        }
    }
}
