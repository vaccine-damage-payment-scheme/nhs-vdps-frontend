'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const dpRepository = require('../../common/schemas/DisabledPersonDetails')
const { findFile, markdown } = require('../../common/utils/markdown')
const config = require('../../common/config')
;
module.exports = () => {
  return {
    render: async (req, res) => {
        try {
              const repo = (await dpRepository()).repository
              const data = await repo.fetch(req.cookies.sessionID)
              data.DpTitle = req.body.DpTitle
              data.DpSurname = req.body.DpSurname
              data.DpMiddlename = req.body.DpMiddlename ? req.body.DpMiddlename : null
              data.DpForename = req.body.DpForename
              await repo.save(data)
               if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
                  res.redirect('other-name')
                  return
                }
                res.redirect('other-dob')
                return
            } catch (e) {
              logRequestStart({
                level: 'error',
                message: e.message
              })
            }
          res.redirect('other-name')
          return

    }
  }
}
