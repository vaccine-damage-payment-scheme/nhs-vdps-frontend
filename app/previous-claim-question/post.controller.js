'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const Data = require('../../common/schemas/Data')
const dataRepository = require('../../common/schemas/Data')
const querystring = require('node:querystring');
module.exports = () => {
  return {
    render: async (req, res) => {
            if(!req.body){
                res.redirect('previous-claim-question')
            }
      try {
      req.session.section = 'YourDetails'
        const repo = (await dataRepository()).repository
        const data = await repo.fetch(req.cookies.sessionID)
        data.PreviousClaim = req.body.PreviousClaim
        await repo.save(data)
        return res.redirect('why-are-you-making-this-claim')
      } catch (e) {
        logRequestStart({
          level: 'error',
          message: e.message
        })
        return res.redirect('previous-claim-question')
      }
    }
  }
}
