'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const { findFile, markdown } = require('../../common/utils/markdown')
const config = require('../../common/config')
const dataRepository = require('../../common/schemas/Data')
const metaRepository = require('../../common/schemas/MetaData')
const healthcareProviderRepository = require('../../common/schemas/HealthcareProviders')
const htmlentities = require('../../common/assets/htmlentities.json')

module.exports = () => {
  return {
    render: async (req, res) => {
      try {


        const repo = (await dataRepository()).repository
        const data = await repo.fetch(req.cookies.sessionID)

        if(!data.AccountVerified || !data.WhyMakingClaim ){
          res.redirect('/')
          return
        }

        let HealthcareProviders = []
        let healthCareProvider
        let healthCareProviderDisplayedAddress

        const hcpRepo = (await healthcareProviderRepository()).repository

        const metaRepo = (await metaRepository()).repository
        const metadata = await metaRepo.fetch(req.cookies.sessionID)

        const healthcareProvidersCount = metadata.NumberOfHealthcareProviders

        for(let i=1; i<=healthcareProvidersCount; i++){
            healthCareProvider = await hcpRepo.fetch(req.cookies.sessionID+":"+i)

            if( healthCareProvider.DisplayOnline ){
                healthCareProviderDisplayedAddress = healthCareProvider.ClinicAddressLine1 + ', ' + healthCareProvider.ClinicAddressLine2 + ', ' +  healthCareProvider.ClinicTownOrCity + ', ' +  healthCareProvider.ClinicCounty + ', ' + healthCareProvider.ClinicPostcode
                healthCareProviderDisplayedAddress = healthCareProviderDisplayedAddress.replace('null,','')
                healthCareProviderDisplayedAddress = healthCareProviderDisplayedAddress.replace('null,','')
                healthCareProviderDisplayedAddress = healthCareProviderDisplayedAddress.replace('null,','')
                healthCareProviderDisplayedAddress = healthCareProviderDisplayedAddress.replace('null,','')
                healthCareProviderDisplayedAddress = healthCareProviderDisplayedAddress.replace(', ,',', ')
                healthCareProviderDisplayedAddress = healthCareProviderDisplayedAddress.replace(', ,',', ')
                healthCareProviderDisplayedAddress = healthCareProviderDisplayedAddress.replace(', ,',', ')
                healthCareProviderDisplayedAddress = healthCareProviderDisplayedAddress.replace(', ,',', ')
                healthCareProvider.DisplayedAddress = healthCareProviderDisplayedAddress
                HealthcareProviders.push(healthCareProvider)
            }
        }


        const templateBody = findFile('healthcare-provider-cya')
        let pageTitle;
        let md = templateBody.toString();
        if(md.includes('{#<pagetitle>')){
            pageTitle = md.split('{#<pagetitle>')[1].split('</pagetitle>')[0]
            md = md.split('</pagetitle>#}')[1]
        }

let errors = null
        if(req.session.errors)
        {
            if(req.session.errors.filter(e => e.route === '/healthcare-provider-cya').length){
                errors = req.session.errors.filter(e => e.route === '/healthcare-provider-cya')
            }
        }
        const markdownRender = markdown(templateBody.toString())
        const params = {
          content: markdownRender,
          file: `./app/healthcare-provider-cya/templates/index.njk`,
          errors: errors,
          data: {
            HealthcareProviders: HealthcareProviders,
            MetaData: metadata.toJSON()
          },
          title: pageTitle ? pageTitle: 'PLEASE SET A PAGE TITLE'
        }
        res.render('app/index/index', params)
      } catch (e) {
        logRequestStart({
          level: 'error',
          message: e.message
        })

        return res.redirect('back')
      }
    }
  }
}

