'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const cdRepository = require('../../common/schemas/ClaimantDetails')
const dataRepository = require('../../common/schemas/Data')
const { formatDateString } = require('../../common/utils/string');


module.exports = () => {
    return {
        render: async (req, res) => {
                 req.session.section = 'YourDetails'
            try {

                // Validation error 
                 if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
                    req.session.body = req.body
                    res.redirect('about-you-dob')
                    return
                  } else {
                    // Validation pass
                    const { dobYear, dobMonth, dobDay } = req.body

                    const repo = (await cdRepository()).repository
                    const cdData = await repo.fetch(req.cookies.sessionID)
                    req.session.errors = null
                    cdData.CdDateOfBirth = formatDateString(dobYear, dobMonth, dobDay)
                    await repo.save(cdData)
                  }


            } catch (e) {
                logRequestStart({
                    level: 'error',
                    message: e.message
                })
            }
            const dataRepo = (await dataRepository()).repository
            const data = await dataRepo.fetch(req.cookies.sessionID)
            if(data.ApplyingFor === 'Myself'){
                res.redirect('about-you-nhs-number')
            } else {
                res.redirect('about-you-address')
            }
            return
        }
    }
}
