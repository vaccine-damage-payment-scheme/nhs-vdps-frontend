const { check, validationResult } = require('express-validator')
const cdRepository = require('../../common/schemas/ClaimantDetails')
const { logRequestStart } = require('../../common/utils/request-logger')
const applicationStatusRepository = require('../../common/schemas/ApplicationStatus')
const validateDateString = require("../../common/utils/validate-date-string")


// this sets the rules on the request body in the middleware
const aboutYouDOBRules = () => {
  return [

  ]
}

// while this function executes the actual rules against the request body
const aboutYouDOBValidation = () => {
  return async (req, res, next) => {
    // execute the rules
    const errors = validationResult(req)

    const {dobYear, dobMonth, dobDay } = req.body

    if (dobDay === '' && dobMonth === '' && dobYear === '') {
      errors.errors.push({
        msg: 'common.errors.E0017',
        param: 'dobDay',
        formgroup: 'dobDate'
      })
    }

    if (errors.errors.length === 0) {
      if (dobDay === '' || (dobDay < 1 && dobDay > 31)) {
        errors.errors.push({
          msg: 'common.errors.E0018',
          param: 'dobDay',
          formgroup: 'dobDate'
        })
      }
      if (dobMonth === '' || (dobMonth < 1 && dobMonth > 12)) {
        errors.errors.push({
          msg: 'common.errors.E0019',
          param: 'dobMonth',
          formgroup: 'dobDate'
        })
      }
      if (dobYear === '') {
        errors.errors.push({
          msg: 'common.errors.E0020',
          param: 'dobYear',
          formgroup: 'dobDate'
        })

      }
      if (dobYear !== '' && dobYear.length != 4) {
        errors.errors.push({
          msg: 'common.errors.E0021',
          param: 'dobYear',
          formgroup: 'dobDate'
        })
      }
    }

    if (errors.errors.length === 0) {
      if (!validateDateString(dobDay, dobMonth, dobYear)) {
        errors.errors.push({
          msg: 'common.errors.E0022',
          param: 'dobDay',
          formgroup: 'dobDate'
        })
      }
    }

    if (errors.errors.length === 0) {
      if (getAge(dobDay, dobMonth, dobYear) < 16) {
        errors.errors.push({
          msg: 'common.errors.E0023',
          param: 'dobDay',
          formgroup: 'dobDate'
        })
      }
    }
    if(req.session.errors){
        req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
    }
    if(req.session.errors){
        req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
            req.session.errors = [...req.session.errors, ...errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))]
        }else{
          req.session.errors = errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))
    }
    if(!errors.isEmpty()){
        const applicationStatusRepo = (await applicationStatusRepository()).repository
        const applicationStatus = await applicationStatusRepo.fetch(req.cookies.sessionID)

        applicationStatus.YourDetails = 'ToDo'

        await applicationStatusRepo.save(applicationStatus)
    }

    // check if any of the request body failed the requirements set in validation rules
    if (errors.isEmpty()) {
        if(req.session.errors){
            req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
        }
        if(!req.session.errors){
            if(!req.session.errors.length){
                req.session.errors = null
            }
        }
    }
    // move to next middleware
    next()
  }
}

function getAge (day, month, year) {
  const today = new Date()
  const birthDate = new Date(year + '-' + month + '-' + day)
  let age = today.getFullYear() - birthDate.getFullYear()
  const m = today.getMonth() - birthDate.getMonth()
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age--
  }
  return age
}

module.exports = {
  aboutYouDOBValidation,
  aboutYouDOBRules
}
