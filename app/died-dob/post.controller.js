'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const dpRepository = require('../../common/schemas/DisabledPersonDetails')
const dataRepository = require('../../common/schemas/Data')
const config = require('../../common/config')
const { findFile, markdown } = require('../../common/utils/markdown')
const querystring = require('node:querystring');
const { formatDateString } = require('../../common/utils/string');

module.exports = () => {
    return {
        render: async (req, res) => {
            try {
                let dobDay = req.body.dobDay
                let dobMonth = req.body.dobMonth
                const dobYear = req.body.dobYear

                if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
                    req.session.body = req.body
                    res.redirect('died-dob')
                    return
                } else {
                    const repo = (await dpRepository()).repository
                    const dpData = await repo.fetch(req.cookies.sessionID)
                    dpData.DpDateOfBirth = formatDateString(dobYear, dobMonth, dobDay)
                    await repo.save(dpData)
                }
            } catch (e) {
                logRequestStart({
                    level: 'error',
                    message: e.message
                })
            }
            if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
                res.redirect('died-dob')
                return
            }
            res.redirect('died-dod')
            return
        }
    }
}
