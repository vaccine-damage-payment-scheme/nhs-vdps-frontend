'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const { findFile, markdown } = require('../../common/utils/markdown')
const config = require('../../common/config')
const metaRepository = require('../../common/schemas/MetaData')
const dataRepository = require('../../common/schemas/Data')
const cdRepository = require('../../common/schemas/ClaimantDetails')
const applicationStatusRepository = require('../../common/schemas/ApplicationStatus')
const claimRepository = require('../../common/schemas/Claim')
const htmlentities = require('../../common/assets/htmlentities.json')

module.exports = () => {
  return {
    render: async (req, res) => {
      try {
        const repo = (await dataRepository()).repository
        const data = await repo.fetch(req.cookies.sessionID)

        if(!data.AccountVerified || !data.WhyMakingClaim ){
            res.redirect('/')
            return
        }

        const metaRepo = (await metaRepository()).repository
        const metadata = await metaRepo.fetch(req.cookies.sessionID)

        const cdRepo = (await cdRepository()).repository
        const cd = await cdRepo.fetch(req.cookies.sessionID)

        const claimRepo = (await claimRepository()).repository
        const claim = await claimRepo.fetch(req.cookies.sessionID)

        if(cd.CdHomeTelNo && cd.CdMobileTelNo){
            cd.DisplayedContactInformation = cd.CdHomeTelNo + ', ' + cd.CdMobileTelNo
        } else if(cd.CdHomeTelNo){
            cd.DisplayedContactInformation = cd.CdHomeTelNo
        } else if(cd.CdMobileTelNo){
            cd.DisplayedContactInformation = cd.CdMobileTelNo
        } else {
            cd.DisplayedContactInformation = 'None provided'
        }


        let displayedAddress = cd.CdAddressLine1 + ', ' + cd.CdAddressLine2 + ', ' +  cd.CdTownOrCity + ', ' +  cd.CdCounty + ', ' + cd.CdPostcode
        displayedAddress = displayedAddress.replace('null, ','')
        displayedAddress = displayedAddress.replace(', ,',', ')
        cd.DisplayedAddress = displayedAddress
        const claimantContactPreferences = claim.ClaimantContactPreferences

        const applicationStatusRepo = (await applicationStatusRepository()).repository
        const applicationStatus = await applicationStatusRepo.fetch(req.cookies.sessionID)
        applicationStatus.YourDetails = 'Completed'
        if(applicationStatus.NominatedPersonDetails !== 'Completed'){
            applicationStatus.NominatedPersonDetails = 'ToDo'
        }
        await applicationStatusRepo.save(applicationStatus)


        const templateBody = findFile('other-about-you-cya')
        let pageTitle;
        let md = templateBody.toString();
        if(md.includes('{#<pagetitle>')){
            pageTitle = md.split('{#<pagetitle>')[1].split('</pagetitle>')[0]
            md = md.split('</pagetitle>#}')[1]
        }
        let errors = null
        if(req.session.errors)
        {
            if(req.session.errors.filter(e => e.route === req.route.path).length){
                errors = req.session.errors.filter(e => e.route === req.route.path)
            }
        }
        
        const markdownRender = markdown(templateBody.toString())
        const params = {
          content: markdownRender,
          file: `./app/other-about-you-cya/templates/index.njk`,
          errors: errors || null,
          data: {
            ClaimantDetails: cd,
            MetaData: metadata,
            Data: data,
            Claim: claim
          },
          title: pageTitle ? pageTitle: 'PLEASE SET A PAGE TITLE'
        }
        res.render('app/index/index', params)
      } catch (e) {
        logRequestStart({
          level: 'error',
          message: e.message
        })

        return res.redirect('back')
      }
    }
  }
}
