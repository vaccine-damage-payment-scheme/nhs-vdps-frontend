'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const { findFile, markdown } = require('../../common/utils/markdown')
const mdRepository = require('../../common/schemas/MetaData')
const docRepository = require('../../common/schemas/Documents')
const { I18n } = require('i18n')
const {generateJWTToken, uploadFile, deleteFile} = require('../../common/utils/fileUpload');

module.exports = () => {
  return {
    render: async (req, res) => {
        const metaRepo = (await mdRepository()).repository
        const MetaData = await metaRepo.fetch(req.cookies.sessionID)
        const metaData = (await metaRepo.fetch(req.cookies.sessionID)).toJSON()
        const files = metaData.NumberOfFiles ? metaData.NumberOfFiles : 0

        if(req.session.errors){
            if(!req.session.errors.length){
                req.session.errors = null
            };
        }
        const error = false;

        if(!req.body.documentId){
            req.session.errors = [{
                msg: `common.errors.E0110`,
                param: 'document',
                route: req.route.path
            }]
            return res.redirect('upload-documents')
        }
        if(!req.body.documentCount){
            req.session.errors = [{
                msg: `common.errors.E0110`,
                param: 'document',
                route: req.route.path
            }]
            return res.redirect('upload-documents')
        }
        try{
            var file = await deleteFile(req.body.documentId);
            if(file.statusCode === 200)
            {
                const metaRepo = (await mdRepository()).repository

                const docRepo = (await docRepository()).repository
                const document = await docRepo.remove(`${req.cookies.sessionID}:${req.body.documentCount}`)
            }
            if(file.statusCode === 400){
                req.session.errors = [{
                    msg:  `common.errors.E0115`,
                    param: 'document',
                    route: req.route.path
                }]
            }
        }catch(e){
            if(e.response){
                if(e.response.data){}
            }


            req.session.errors = [{
                msg: `common.errors.E0115`,
                param: 'file',
                route: req.route.path
            }]
        }
        return res.redirect('upload-documents')
    }
  }
}
