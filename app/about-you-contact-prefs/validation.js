const { check, validationResult } = require('express-validator')
const claimRepository = require('../../common/schemas/Claim')
const applicationStatusRepository = require('../../common/schemas/ApplicationStatus')

// this sets the rules on the request body in the middleware
const aboutYouContactPrefsRules = () => {
  return [
    check('ClaimantContactPreferences').isIn(['Email', 'Phone', 'Letter']).withMessage('common.errors.E0015')
  ]
}

// while this function executes the actual rules against the request body
const aboutYouContactPrefsValidation = () => {
  return async(req, res, next) => {

    const repo = (await claimRepository()).repository
    const data = await repo.fetch(req.cookies.sessionID)
    data.ClaimantContactPreferences = [];
    await repo.save(data);
    // execute the rules
    const errors = validationResult(req)
    if(!errors.isEmpty()){
            errors.errors = errors.errors.map(e => Object.assign({},e,{ param:'Email', route: req.route.path}))
    }
    if(!errors.isEmpty()){
            const applicationStatusRepo = (await applicationStatusRepository()).repository
            const applicationStatus = await applicationStatusRepo.fetch(req.cookies.sessionID)


        applicationStatus.YourDetails = 'ToDo'

        await applicationStatusRepo.save(applicationStatus)
    }

     if (errors.isEmpty()) {
        if(req.session.errors){
            if(req.session.errors.length){
                req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
            }
        }
        if(req.session.errors){
            if(!req.session.errors.length){
                req.session.errors = null
            }
        }
    }
    if (!errors.isEmpty()) {
            req.session.errors = errors.errors.map(err => Object.assign({},err,{param:'Email', route: req.route.path}))
            if(req.session.errors){
        req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
            req.session.errors = [...req.session.errors, ...errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))]
                }else{
                  req.session.errors = errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))
            }
            res.redirect('about-you-contact-prefs')
        return
    }
    // check if any of the request body failed the requirements set in validation rules
        if(!req.session.errors || !req.session.errors.length){

            req.session.errors = null
        }

      // heres where you send the errors or do whatever you please with the error, in  your case
    // if everything went well, and all rules were passed, then move on to the next middleware
    next()
  }
}
module.exports = {
  aboutYouContactPrefsValidation,
  aboutYouContactPrefsRules
}
