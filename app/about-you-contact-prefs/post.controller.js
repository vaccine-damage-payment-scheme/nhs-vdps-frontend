'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const claimRepository = require('../../common/schemas/Claim')
const dataRepository = require('../../common/schemas/Data')

module.exports = () => {
  return {
    render: async (req, res) => {
          req.session.section = 'YourDetails'
  try {
        const claimRepo = (await claimRepository()).repository
        const claim = await claimRepo.fetch(req.cookies.sessionID)
        claim.ClaimantContactPreferences = (typeof req.body.ClaimantContactPreferences === 'string') ? [JSON.stringify({Name: req.body.ClaimantContactPreferences})] : req.body.ClaimantContactPreferences.map(cp => JSON.stringify({Name: cp}))
        await claimRepo.save(claim)
        if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
          res.redirect('back')
          return
        }
        if(!req.session.errors || !req.session.errors.length){
                     req.session.errors = null
        }
        const dataRepo = (await dataRepository()).repository
        const data = await dataRepo.fetch(req.cookies.sessionID)
        if (data.ApplyingFor === "Myself")
            res.redirect('about-you-cya')
        else
           res.redirect('other-about-you-cya')
      } catch (e) {
        logRequestStart({
          level: 'error',
          message: e.message
        })
        return res.redirect('about-you-contact-prefs')
      }
    }
  }
}
