'use strict'
const applicationStatusRepository = require('../../common/schemas/ApplicationStatus')
const vacRepository = require('../../common/schemas/Vaccinations')
const metaRepository = require('../../common/schemas/MetaData')
const { logRequestStart } = require('../../common/utils/request-logger')
const config = require('../../common/config')
const { findFile, markdown } = require('../../common/utils/markdown')
const querystring = require('node:querystring');
const { formatDateString } = require('../../common/utils/string');

module.exports = () => {
    return {
        render: async (req, res) => {
            try {
                req.session.section = 'VaccinesReceived'

                const metaRepo = (await metaRepository()).repository
                const metaData = await metaRepo.fetch(req.cookies.sessionID)

                const appStatusRepo = (await applicationStatusRepository()).repository
                const applicationStatus = await appStatusRepo.fetch(req.cookies.sessionID)

                let vaccineCount

                if(req.body.vaccineCount){
                    vaccineCount = req.body.vaccineCount
                } else {
                    vaccineCount = metaData.NumberOfVaccines
                }
                vaccineCount =  vaccineCount ? vaccineCount : 1

                const repo = (await vacRepository()).repository
                const data = await repo.fetch(req.cookies.sessionID + ':' + vaccineCount)

                // 'I don't know option always take priority if user has chosen
                if(req.body.PolioDatesUnknown){
                    metaData.DateUnknown = req.body.PolioDatesUnknown
                    data.VaccinationDate = ""
                } else {

                    if(req.session.errors){
                        if (req.session.errors.length > 0){
                            req.session.body = req.body

                            return res.redirect('polio-dates')
                        }
                    } 
                    metaData.DateUnknown = null;
                    const {vaccineYear, vaccineMonth, vaccineDay} = req.body

                    data.VaccinationDate = formatDateString(vaccineYear, vaccineMonth, vaccineDay)
    
                }

                data.VaccinationType = 'Polio (Poliomyelitis - orally administered)'
                data.DisplayVaccination = true
                await repo.save(data)

                metaData.NumberOfVaccines = Number(vaccineCount)
                await metaRepo.save(metaData)

                applicationStatus.DatesOfVaccination = 'Completed'
                applicationStatus.WhatHappenedNext = 'ToDo'

                await appStatusRepo.save(applicationStatus)

                res.redirect('polio-notes')
            } catch (e) {
                logRequestStart({
                    level: 'error',
                    message: e.message
                })
            }
            /*render: async (req, res) => {
                if(req.session.errors){
                    res.redirect('polio-dates')
                    return
                }
            }*/
        }
    }
}
