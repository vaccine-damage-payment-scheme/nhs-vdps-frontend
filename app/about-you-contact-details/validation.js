const { check, validationResult } = require('express-validator')
const cdRepository = require('../../common/schemas/ClaimantDetails')
const { logRequestStart } = require('../../common/utils/request-logger')
const applicationStatusRepository = require('../../common/schemas/ApplicationStatus')

// this sets the rules on the request body in the middleware
const aboutYouContactDetailsRules = () => {
  return [
    check('CdMobileTelNo')
        .trim()
        .matches(/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/)
      .withMessage('common.errors.E0012')
        .bail()
        //.isMobilePhone('en-GB')
        .isLength({ min: 11 })
      .withMessage('common.errors.E0013')
        .isLength({ max: 25 })
      .withMessage('common.errors.E0014')
  ]
}

// while this function executes the actual rules against the request body
const aboutYouContactDetailsValidation = () => {
  return async (req, res, next) => {
    // execute the rules
    const errors = validationResult(req)
if(req.session.errors){
        req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
            req.session.errors = [...req.session.errors, ...errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))]
        }else{
          req.session.errors = errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))
    }
    if(!errors.isEmpty()){
            const applicationStatusRepo = (await applicationStatusRepository()).repository
            const applicationStatus = await applicationStatusRepo.fetch(req.cookies.sessionID)


        applicationStatus.YourDetails = 'ToDo'

        await applicationStatusRepo.save(applicationStatus)
    }

   // check if any of the request body failed the requirements set in validation rules
    if (errors.isEmpty()) {
if(req.session.errors){
            req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
        }
        if(!req.session.errors){
            if(!req.session.errors.length){
            req.session.errors = null
        }
        }
    // heres where you send the errors or do whatever you please with the error, in  your case

    }
    // if everything went well, and all rules were passed, then move on to the next middleware

    next()
  }
}
module.exports = {
  aboutYouContactDetailsValidation,
  aboutYouContactDetailsRules
}
