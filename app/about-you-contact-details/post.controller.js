'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const cdRepository = require('../../common/schemas/ClaimantDetails')


module.exports = () => {
  return {
    render: async (req, res) => {
         req.session.section = 'YourDetails'
 try {
          const cdRepo = (await cdRepository()).repository
          const cd = await cdRepo.fetch(req.cookies.sessionID)
          
          cd.CdMobileTelNo  = req.body.CdMobileTelNo;
          cd.CdHomeTelNo  = req.body.CdMobileTelNo;
          cd.CdWorkTelNo  = req.body.CdMobileTelNo;
          await cdRepo.save(cd)

        } catch (e) {
          logRequestStart({
            level: 'error',
            message: e.message
          })
        }
         if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
            res.redirect('about-you-contact-details')
            return
          }

        res.redirect('about-you-contact-prefs')


    }
  }
}
