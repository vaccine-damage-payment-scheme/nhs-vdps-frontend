'use strict'
const dataRepository = require('../../common/schemas/Data')
const repRepository = require('../../common/schemas/Representative')
const applicationStatusRepository = require('../../common/schemas/ApplicationStatus')

module.exports = () => {
  return {
    render: async (req, res) => {
        if(!req.body){
            res.redirect('representative-question')
        }
        try {
            const repo = (await dataRepository()).repository
            const data = await repo.fetch(req.cookies.sessionID)

            const repRepo = (await repRepository()).repository
            const representative = await repRepo.fetch(req.cookies.sessionID)

            representative.RepresentativeQuestion = req.body.RepresentativeFlag
            await repRepo.save(representative)
          req.session.section = 'NominatedPersonDetails'

            if (req.body.RepresentativeFlag === 'Yes') {
                const applicationStatusRepo = (await applicationStatusRepository()).repository
                const applicationStatus = await applicationStatusRepo.fetch(req.cookies.sessionID)
                applicationStatus.NominatedPersonDetails = 'ToDo'
                await applicationStatusRepo.save(applicationStatus)
                res.redirect('representative-name')
                return
            } else {
                const applicationStatusRepo = (await applicationStatusRepository()).repository
                const applicationStatus = await applicationStatusRepo.fetch(req.cookies.sessionID)
                applicationStatus.NominatedPersonDetails = 'Completed'
                if(data.ApplyingFor === 'Myself') {
                    if(data.WhyMakingClaim === 'mother'){
                       if(applicationStatus.VaccinatedPersonDetails !== 'Completed'){
                            applicationStatus.VaccinatedPersonDetails = 'ToDo'
                        }
                    } else if(data.WhyMakingClaim === 'polio'){
                        if(applicationStatus.VaccinatedPersonDetails !== 'Completed'){
                            applicationStatus.VaccinatedPersonDetails = 'ToDo'
                        }
                    } else {
                        if(applicationStatus.VaccinesReceived !== 'Completed'){
                            applicationStatus.VaccinesReceived = 'ToDo'
                        }
                    }
                } else {
                    if(data.ApplyingFor === 'Under16' || data.ApplyingFor === 'Disabled'  || data.ApplyingFor === 'Deceased' ){
                         if(applicationStatus.DisabledPersonDetails !== 'Completed'){
                            applicationStatus.DisabledPersonDetails = 'ToDo'
                        } else if(data.WhyMakingClaim === 'mother') {
                            if(applicationStatus.VaccinatedPersonDetails !== 'Completed'){
                                applicationStatus.VaccinatedPersonDetails = 'ToDo'
                            }
                        } else if(data.WhyMakingClaim === 'polio'){
                            if(applicationStatus.DoctorDetails !== 'Completed'){
                                applicationStatus.DoctorDetails = 'ToDo'
                            }
                        }
                    }
                }
                await applicationStatusRepo.save(applicationStatus)
                res.redirect('your-application')
                return
            }
        } catch (e) {
            logRequestStart({
                level: 'error',
                message: e.message
            })
            return res.redirect('representative-question')
        }
    }
  }
}
