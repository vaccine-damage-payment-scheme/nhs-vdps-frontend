const { check, validationResult } = require('express-validator')

// this sets the rules on the request body in the middleware
const representativeQuestionRules = () => {
  return [

  ]
}

// while this function executes the actual rules against the request body
const representativeQuestionValidation = () => {
  return (req, res, next) => {
    // execute the rules
    const errors = validationResult(req)
    if (req.body.RepresentativeFlag === undefined) {
      errors.errors.push({
         msg: 'common.errors.E0016',
        param: 'Yes'
      })
    }
        if(req.session.errors){
        req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
            req.session.errors = [...req.session.errors, ...errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))]
        }else{
          req.session.errors = errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))
    }
    // check if any of the request body failed the requirements set in validation rules
    if (!errors.isEmpty()) {
      // heres where you send the errors or do whatever you please with the error, in  your case
      res.redirect('representative-question')
      return
    }

    next()
  }
}
module.exports = {
  representativeQuestionRules,
  representativeQuestionValidation
}
