'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const representativeRepository = require('../../common/schemas/Representative')
const { findFile, markdown } = require('../../common/utils/markdown')
const config = require('../../common/config')
;
module.exports = () => {
  return {
    render: async (req, res) => {
          req.session.section = 'NominatedPersonDetails'

        try {
              const repo = (await representativeRepository()).repository
              const data = await repo.fetch(req.cookies.sessionID)
              data.RepresentativeMobileTelNo = req.body.RepresentativeMobileTelNo ? req.body.RepresentativeMobileTelNo : null
              data.RepresentativeHomeTelNo = req.body.RepresentativeHomeTelNo ? req.body.RepresentativeHomeTelNo : null
              data.RepresentativeWorkTelNo = req.body.RepresentativeWorkTelNo ? req.body.RepresentativeWorkTelNo : null
              data.RepresentativeEmailAddress = req.body.RepresentativeEmailAddress
              await repo.save(data)
               if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
                  res.redirect('representative-contact')
                  return
                }
              res.redirect('representative-cya')
                return
            } catch (e) {
              logRequestStart({
                level: 'error',
                message: e.message
              })
            }
          res.redirect('representative-contact')
          return

    }
  }
}
