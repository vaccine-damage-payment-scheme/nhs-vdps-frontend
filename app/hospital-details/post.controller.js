'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const hospitalRepository = require('../../common/schemas/Hospitals')
const metaRepository = require('../../common/schemas/MetaData')
const { findFile, markdown } = require('../../common/utils/markdown')
const config = require('../../common/config')
;
module.exports = () => {
  return {
    render: async (req, res) => {
                      req.session.section = 'HospitalAttendance'
 try {
            const metaRepo = (await metaRepository()).repository
            const metaData = await metaRepo.fetch(req.cookies.sessionID)
            let count
            if(req.body.hospitalCount){
                count =  Number(req.body.hospitalCount)
            } else {
                count = metaData.NumberOfHospitals;
                if(!count){
                    count = 1
                } else {
                    count = count+1
                }
            }

            const repo = (await hospitalRepository()).repository
            const data = await repo.fetch(req.cookies.sessionID + ':' + count)

            data.HospitalName = req.body.hospitalName
            data.HospitalAddressLine1 = req.body.hospitalAddressLine1
            data.HospitalAddressLine2 = req.body.hospitalAddressLine2 ? req.body.hospitalAddressLine2 : null
            data.HospitalTownOrCity = req.body.hospitalTownOrCity
            data.HospitalCounty = req.body.hospitalCounty ? req.body.hospitalCounty : null
            data.HospitalPostcode = req.body.hospitalPostcode
            data.HospitalConsultant = req.body.hospitalConsultant ? req.body.hospitalConsultant : null
            data.HospitalConsultantTelNo = req.body.hospitalConsultantTelNo ? req.body.hospitalConsultantTelNo : null
            data.HospitalConsultantEmail = req.body.hospitalConsultantEmail ? req.body.hospitalConsultantEmail : null
            data.HospitalCount = count
            data.DisplayOnline = false
            await repo.save(data)

            if(!req.body.hospitalCount){
                metaData.NumberOfHospitals = count
                await metaRepo.save(metaData)
            }

            if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
                res.redirect('hospital-details?hospitalCount='+count)
                return
            }
            data.DisplayOnline = true
            await repo.save(data)

            res.redirect('hospital-cya')
            return
        } catch (e) {
            logRequestStart({
                level: 'error',
                message: e.message
            })
        }
        res.redirect('hospital-details')
        return

    }
  }
}
