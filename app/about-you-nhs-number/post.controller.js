'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const dataRepository = require('../../common/schemas/Data')
const { findFile, markdown } = require('../../common/utils/markdown')
const config = require('../../common/config')
const querystring = require('node:querystring');

module.exports = () => {
  return {
    render: async (req, res) => {
        req.session.section = 'YourDetails'
   try {
          const repo = (await dataRepository()).repository
          const data = await repo.fetch(req.cookies.sessionID)
          data.NhsNumber = req.body.NhsNumber
          await repo.save(data)
        } catch (e) {
          logRequestStart({
            level: 'error',
            message: e.message
          })
        }
     if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
        res.redirect('about-you-nhs-number')
        return
      }
      res.redirect('about-you-address')
    }
  }
}
