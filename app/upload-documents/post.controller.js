'use strict'
const mdRepository = require('../../common/schemas/MetaData')
const dataRepository = require('../../common/schemas/Data')
const cdRepository = require('../../common/schemas/ClaimantDetails')
const { logRequestStart } = require('../../common/utils/request-logger')
const config = require('../../common/config')
const { findFile, markdown } = require('../../common/utils/markdown')
const docRepository = require('../../common/schemas/Documents')
const { I18n } = require('i18n')
const {generateJWTToken, uploadFile, deleteFile} = require('../../common/utils/fileUpload');


module.exports = () => {
  return {
    render: async (req, res) => {

        const metaRepo = (await mdRepository()).repository
        const MetaData = await metaRepo.fetch(req.cookies.sessionID)
        const metaData = (await metaRepo.fetch(req.cookies.sessionID)).toJSON()
        const files = metaData.NumberOfFiles ? metaData.NumberOfFiles : 0

        if(req.session.errors){
            if(!req.session.errors.length){
                req.session.errors = null
            };
        }
        const error = false;
        const regexp = new RegExp('^.*\.(jpg|JPG|jpeg|JPEG|png|PNG|pdf|PDF)$');
        const maxAllowedSize = 4 * 1024 * 1024;

        if(!req.files){
            req.session.errors = [{
                msg: `common.errors.E0110`,
                param: 'document',
                route: req.route.path
            }]
            return res.redirect('upload-documents')
        }

        if(req.files && req.files.document){
            if(req.files.document.size > maxAllowedSize){
                req.session.errors = [{
                    msg: `common.errors.E0111`,
                    param: 'document',
                    route: req.route.path
                }]
                return res.redirect('upload-documents')

            }
            if(!regexp.test(req.files.document.name)){
                req.session.errors = [{
                    msg: `common.errors.E0112`,
                    param: 'document',
                    route: req.route.path
                }]
                return res.redirect('upload-documents')
            }

            if(files >= 10){
                req.session.errors = [{
                    msg: `common.errors.E0113`,
                    param: 'document',
                   route: req.route.path
                }]
                return res.redirect('upload-documents')
            }

            try{
                var file = await uploadFile(req.sessionID,req.files.document.mimetype,req.files.document);
                const mimetype = req.files.document.mimetype;
                if(file.statusCode === 200)
                {
                    const metaRepo = (await mdRepository()).repository
                    const fileCount = files + 1

                    const docRepo = (await docRepository()).repository
                    const newDocument = await docRepo.fetch(`${req.cookies.sessionID}:${fileCount}`)
                    newDocument.DocumentName = req.files.document.name
                    newDocument.DocumentID =  file.data.fileid
                    newDocument.DocumentType = (mimetype.includes('pdf') || mimetype.includes('PDF')) ? 'pdf' : (mimetype.includes('png') || mimetype.includes('PNG')) ? 'png' : (mimetype.includes('jpg') || mimetype.includes('jpeg') || mimetype.includes('JPG') || mimetype.includes('JPEG')) ? 'jpg' : mimetype
                    newDocument.DocumentPath = file.data.filepath
                    newDocument.DocumentStatus = "New"
                    newDocument.DocumentCount = fileCount
                    newDocument.UploadStatus = `${file.data.recordstatus} ${file.data.recordsubstatus}`
                    await docRepo.save(newDocument)

                    const newMetaData = await metaRepo.fetch(req.cookies.sessionID)
                    newMetaData.NumberOfFiles = fileCount
                    await metaRepo.save(newMetaData)
                }

                if(file.statusCode === 400){
                        req.session.errors = [{
                            msg:  `common.errors.E0114`,
                            param: 'document',
                            route: req.route.path
                        }]
                        return res.redirect('upload-documents')
                }
            }catch(e){
                if(e.response){
                    if(e.response.data){}
                }
                req.session.errors = [{
                    msg: `common.errors.E0114`,
                    param: 'file',
                    route: req.route.path
                }]
                return res.redirect('upload-documents')
            }
        }

        return res.redirect('upload-documents')
    }
  }
}
