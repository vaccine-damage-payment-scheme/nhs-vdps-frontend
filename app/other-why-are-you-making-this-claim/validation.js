const { check, validationResult } = require('express-validator')

// this sets the rules on the request body in the middleware
const otherWhyAreYouMakingThisClaimRules = () => {
  return [

  ]
}

// while this function executes the actual rules against the request body
const otherWhyAreYouMakingThisClaimValidation = () => {
  return (req, res, next) => {
    // execute the rules
    const errors = validationResult(req)
    if (req.body.WhyMakingClaim === undefined) {
      errors.errors.push({
        msg: 'common.errors.E0016',
        param: 'receivedVaccine',
        route: req.route.path,
        section: 'WhyMakingClaim'
      })
    }

    if(req.session.errors){
        req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
            req.session.errors = [...req.session.errors, ...errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))]
        }else{
          req.session.errors = errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))
    }
    // check if any of the request body failed the requirements set in validation rules
    if (!errors.isEmpty()) {
      // heres where you send the errors or do whatever you please with the error, in  your case
        req.session.errors = req.session.errors.filter(e => e.route === req.route.path)
        res.redirect('other-why-are-you-making-this-claim')
      return
    }
    if (errors.isEmpty()) {
        if(req.session.errors){
            req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
        }
                    if(req.session.errors){
if(!req.session.errors.length){
            req.session.errors = null
        }
        }
    }
    next()
  }
}
module.exports = {
  otherWhyAreYouMakingThisClaimValidation,
  otherWhyAreYouMakingThisClaimRules
}
