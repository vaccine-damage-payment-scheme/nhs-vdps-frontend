'use strict'
const { requestVerification, notifyClient } = require('../../common/utils/claim')
const { logRequestStart } = require('../../common/utils/request-logger')
const applicationStatusRepository = require('../../common/schemas/ApplicationStatus')
const dataRepository = require('../../common/schemas/Data')
const metaRepository = require('../../common/schemas/MetaData')
const {verifyClaim} = require('../../common/utils/claim')
;
module.exports = () => {
  return {
    render: async (req, res) => {
        if(!req.body){
            res.redirect('user-verify')
        }
      try{
       // confirm notification code is correct
        const metaRepo = (await metaRepository()).repository
        const MetaData = await metaRepo.fetch(req.cookies.sessionID)
        const code = req.body.code
        const meta = (await metaRepo.fetch(req.cookies.sessionID)).toJSON()

        if (!Number(code)){
            req.session.errors = [{param: 'code', msg: `common.errors.E0118`, route: '/user-verify'}]
            return res.redirect('user-verify')
        }

        const verify = await verifyClaim({email: meta.Email, code: code})
        if(verify.data.success){
            try{
                // lets setup the initial application statuses.
                const applicationStatusRepo = (await applicationStatusRepository()).repository
                const applicationStatus = await applicationStatusRepo.fetch(req.cookies.sessionID)
                applicationStatus.YourDetails = 'ToDo'
                applicationStatus.NominatedPersonDetails = 'Cannot-start-yet'
                applicationStatus.DisabledPersonDetails = 'Cannot-start-yet'
                applicationStatus.VaccinatedPersonDetails = 'Cannot-start-yet'
                applicationStatus.VaccinesReceived = 'Cannot-start-yet'
                applicationStatus.DatesOfVaccination = 'Cannot-start-yet'
                applicationStatus.WhatHappenedNext = 'Cannot-start-yet'
                applicationStatus.DoctorDetails = 'Cannot-start-yet'
                applicationStatus.HospitalAttendance = 'Cannot-start-yet'
                applicationStatus.HealthClinicAttendance = 'Cannot-start-yet'
                applicationStatus.Declaration = 'Cannot-start-yet'
                await applicationStatusRepo.save(applicationStatus)

                const dataRepo = (await dataRepository()).repository
                const data = await dataRepo.fetch(req.cookies.sessionID)
                data.AccountVerified = true
                await dataRepo.save(data)

                MetaData.ResendCodeCount = 0
                metaRepo.save(MetaData)
                if(req.session.errors){
                    if(!req.session.errors.length){
                        req.session.errors = null
                    }else{
                        req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
                    }
                }
                return res.redirect('who-are-you-applying-for')
            }catch(e){
                logRequestStart({
                    level: 'error',
                    message: e.message
                })
                req.session.errors = [{param: 'code', msg: `common.errors.E0119`, route: '/user-verify'}]
                return res.redirect('user-verify')
            }
        }else{
            logRequestStart({
                level: 'error',
                message: verify.data.message
            })
            req.session.errors = [{param: 'code', msg: `common.errors.E0119`, route: '/user-verify'}]
            return res.redirect('user-verify')
        }
      }catch(e) {
        logRequestStart({
          level: 'error',
          message: e.message
        })
        req.session.errors = [{param: 'code', msg: `common.errors.E0118`, route: '/user-verify'}]
        return res.redirect('user-verify')
      }
    }
  }
}
