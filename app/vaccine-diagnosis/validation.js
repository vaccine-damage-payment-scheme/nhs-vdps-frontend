module.exports.vaccineDiagnosisValidation = () => {
  return (req, res, next) => {
    const errors = [];

    // Empty diagnosis, or an array contains empty strings 
    if (!req.body.diagnosis || (Array.isArray(req.body.diagnosis) && req.body.diagnosis.some(element => element === ''))) {
      errors.push({
        route: req.route.path,
        msg: 'common.errors.E0016',
        param: 'diagnosis'
      });
    }

    if (errors.length > 0) {
      req.session.errors = errors;
      return res.redirect(req.route.path);
    }

    delete req.session.errors;

    next();
  };
};