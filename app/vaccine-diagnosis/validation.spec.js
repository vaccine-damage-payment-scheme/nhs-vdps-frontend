const { vaccineDiagnosisValidation } = require('./validation');
const { req, res, next } = require('express');

describe('vaccineDiagnosisValidation', () => {

    it('should pass validation for "None of the above" selected', () => {
        req.body.diagnosis = 'None of the above';
        vaccineDiagnosisValidation()(req, res, next);
        expect(next).toHaveBeenCalledTimes(1);
    });

    it('should pass validation for multiple items selected', () => {
        req.body.diagnosis = ['Myocarditis', 'Pericarditis', 'Narcolepsy'];
        vaccineDiagnosisValidation()(req, res, next);
        expect(next).toHaveBeenCalledTimes(1);
    });

    it('should fail validation for empty diagnosis', () => {
        req.body.diagnosis = '';
        vaccineDiagnosisValidation()(req, res, next);
        expect(req.session.errors).toEqual([
            {
                "msg": "common.errors.E0016",
                "param": "diagnosis",
                "route": "/path",
            },
        ]);
        expect(res.redirect.mock.calls[0][0]).toEqual('/path');
    });

    it('should fail validation for empty array diagnosis', () => {
        req.body.diagnosis = [''];
        vaccineDiagnosisValidation()(req, res, next);
        expect(req.session.errors).toEqual([
            {
                "msg": "common.errors.E0016",
                "param": "diagnosis",
                "route": "/path",
            },
        ]);
        expect(res.redirect.mock.calls[0][0]).toEqual('/path');
    });
});
