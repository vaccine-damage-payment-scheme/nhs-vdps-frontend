const { check, validationResult } = require('express-validator')

// this sets the rules on the request body in the middleware
const whoAreYouApplyingForRules = () => {
  return [

  ]
}

// while this function executes the actual rules against the request body
const whoAreYouApplyingForValidation = () => {
  return async (req, res, next) => {
    // execute the rules
    const errors = validationResult(req)
    if (!req.body.ApplyingFor) {
      errors.errors.push({
        msg: 'common.errors.E0107',
        param: 'Myself',
        route: req.route.path,
        section: 'ApplyingFor'
      })
    }

    if (req.body.ApplyingFor === 'SomeoneElse' && !req.body.OnBehalfOf && req.session.passage === "onBehalfOf") {
      errors.errors.push({
         msg: 'common.errors.E0016',
        param: 'under16',
        route: req.route.path,
        section: 'OnBehalfOf'
      })
    }
    if(req.session.errors){
        req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
            req.session.errors = [...req.session.errors, ...errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))]
        }else{
          req.session.errors = errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))
    }
    // check if any of the request body failed the requirements set in validation rules

    if (errors.isEmpty()) {
        if(req.session.errors){
            req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
        }
        if(!req.session.errors.length){
            req.session.errors = null
        }
    }
    // if everything went well, and all rules were passed, then move on to the next middleware
    next()
  }
}
module.exports = {
  whoAreYouApplyingForValidation,
  whoAreYouApplyingForRules
}
