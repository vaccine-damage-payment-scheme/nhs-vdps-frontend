'use strict'

const vacRepository = require('../../common/schemas/Vaccinations')
const metaRepository = require('../../common/schemas/MetaData')
const { logRequestStart } = require('../../common/utils/request-logger')
const config = require('../../common/config')
const { findFile, markdown } = require('../../common/utils/markdown')
const querystring = require('node:querystring');

module.exports = () => {
    return {
        render: async (req, res) => {
                req.session.section = 'VaccinesReceived'
  try {

                if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
                    req.session.body = req.body
                    res.redirect('add-vaccine-details')
                    return
                }

                const metaRepo = (await metaRepository()).repository
                const metaData = await metaRepo.fetch(req.cookies.sessionID)

                let vaccineCount

                if(req.body.vaccineCount){
                    vaccineCount  = req.body.vaccineCount
                } else {
                    vaccineCount = metaData.NumberOfVaccines
                }

                let vaccineDay = req.body.vaccineDay ? req.body.vaccineDay : 0
                let vaccineMonth = req.body.vaccineMonth ? req.body.vaccineMonth : 0
                const vaccineYear = req.body.vaccineYear ? req.body.vaccineYear : 0
                if (vaccineDay && vaccineDay.length === 1) { vaccineDay = '0' + vaccineDay }
                if (vaccineMonth && vaccineMonth.length === 1) { vaccineMonth = '0' + vaccineMonth }
                const repo = (await vacRepository()).repository
                const data = await repo.fetch(req.cookies.sessionID + ':' + vaccineCount)

                data.VaccinationDate = vaccineYear+'-'+vaccineMonth+'-'+vaccineDay

                let vaccine
                let vaccinationFound = false

                for(let i=1; i<= metaData.NumberOfVaccines; i++){
                    vaccine = await repo.fetch(req.cookies.sessionID + ":" + i)
                    if(vaccine.VaccinationType === data.VaccinationType && vaccine.VaccinationDate === data.VaccinationDate && vaccine.DisplayVaccination){
                        vaccinationFound = true
                    }
                }

                if(!vaccinationFound){
                    data.DisplayVaccination = true
                } else {
                    req.session.info = 'common.errors.E0120'
                    data.DisplayVaccination = false
                }

                await repo.save(data)
                if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
                    res.redirect('add-vaccine-details')
                    return
                }
                res.redirect('add-vaccine-summary')
            } catch (e) {
                logRequestStart({
                    level: 'error',
                    message: e.message
                })
            }
        }
    }
}
