const { check, validationResult } = require('express-validator')
const { logRequestStart } = require('../../common/utils/request-logger')
const querystring = require('node:querystring');
const validateDateString = require("../../common/utils/validate-date-string")

// this sets the rules on the request body in the middleware
const verificationDOBRules = () => {
  return [

  ]
}

// while this function executes the actual rules against the request body
const verificationDOBValidation = () => {
  return async (req, res, next) => {
    //req.session.errors = null
    
    // execute the rules
    const errors = validationResult(req)
    const errorsInput = []

    const dobDay = req.body.dobDay
    const dobMonth = req.body.dobMonth
    const dobYear = req.body.dobYear

    if (dobDay === '' && dobMonth === '' && dobYear === '') {
      errors.errors.push({
        msg: 'common.errors.E0099',
        param: 'dobDay',
        formgroup: 'dobDate'
      })
    }

    if (errors.errors.length === 0) {
      if (dobDay === '') {
        errors.errors.push({
          msg: 'common.errors.E0100',
          param: 'dobDay',
          formgroup: 'dobDate'
        })
      }
      if (dobMonth === '') {
        errors.errors.push({
          msg: 'common.errors.E0101',
          param: 'dobMonth',
          formgroup: 'dobDate'
        })
      }
      if (dobYear === '') {
        errors.errors.push({
          msg: 'common.errors.E0102',
          param: 'dobYear',
          formgroup: 'dobDate'
        })

      }
      if (dobYear !== '' && dobYear.length != 4) {
        errors.errors.push({
          msg: 'common.errors.E0103',
          param: 'dobYear',
          formgroup: 'dobDate'
        })
      }
    }

    if (errors.errors.length === 0) {
      if (!validateDateString(dobDay, dobMonth, dobYear)) {
        errors.errors.push({
          msg: 'common.errors.E0104',
          param: 'dobDay',
          formgroup: 'dobDate'
        })
      }
    }

    if (errors.errors.length === 0) {
      if (getAge(dobDay, dobMonth, dobYear) < 16) {
        errors.errors.push({
          msg: 'common.errors.E0023',
          param: 'dobDay',
          formgroup: 'dobDate'
        })
      }
    }

    if(req.session.errors){
        req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
            req.session.errors = [...req.session.errors, ...errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))]
        }else{
          req.session.errors = errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))
    }
    // check if any of the request body failed the requirements set in validation rules
    if (errors.isEmpty()) {
        if(req.session.errors){
            req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
        }
        if(req.session.errors){
            if(!req.session.errors.length){
                req.session.errors = null
            }
        }
    }
    // move to next middleware
    next()
  }
}

function getAge (day, month, year) {
  const today = new Date()
  const birthDate = new Date(year + '-' + month + '-' + day)
  let age = today.getFullYear() - birthDate.getFullYear()
  const m = today.getMonth() - birthDate.getMonth()
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age--
  }
  return age
}

module.exports = {
  verificationDOBValidation,
  verificationDOBRules
}
