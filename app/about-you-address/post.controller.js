'use strict'
const dataRepository = require('../../common/schemas/Data')
const cdRepository = require('../../common/schemas/ClaimantDetails')
const { logRequestStart } = require('../../common/utils/request-logger')


module.exports = () => {
  return {
    render: async (req, res) => {

        try {
          const dataRepo = (await dataRepository()).repository
          const data = await dataRepo.fetch(req.cookies.sessionID)
      req.session.section = 'YourDetails'

          if (data.ApplyingFor) {
            // copy CD to VDP if ApplyingFor = self
          }
          const cdRepo = (await cdRepository()).repository
          const cd = await cdRepo.fetch(req.cookies.sessionID)
          cd.CdAddressLine1 = req.body.CdAddressLine1
          cd.CdAddressLine2 = req.body.CdAddressLine2
          cd.CdTownOrCity = req.body.CdTownOrCity
          cd.CdCounty = req.body.CdCounty ? req.body.CdCounty : null
          cd.CdPostcode = req.body.CdPostcode
          await cdRepo.save(cd)
          if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
            res.redirect('about-you-address')
            return
          }

            res.redirect('about-you-contact-details')
        } catch (e) {
          logRequestStart({
            level: 'error',
            message: e.message
          })

        }
    }
  }
}
