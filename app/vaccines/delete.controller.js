'use strict'
const vacRepository = require('../../common/schemas/Vaccinations')
const { logRequestStart } = require('../../common/utils/request-logger')
const config = require('../../common/config')
const { findFile, markdown } = require('../../common/utils/markdown')

module.exports = () => {
  return {
    render: async (req, res) => {
        res.redirect('add-vaccine-summary')
         try {
            const repo = (await vacRepository()).repository
            const data = await repo.fetch(req.cookies.sessionID+':'+req.query.vaccineCount)
            data.DisplayVaccination = false
            await repo.save(data)
             if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
                res.redirect('add-vaccine-details')
                return
              }
            return res.redirect('add-vaccine-summary')
        } catch (e) {
            logRequestStart({
                level: 'error',
                message: e.message
            })
        }
        render: async (req, res) => {
            if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
                res.redirect('add-vaccine-details')
                return
            }
        }
    }
  }
}
