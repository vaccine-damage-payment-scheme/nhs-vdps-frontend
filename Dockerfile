FROM --platform=linux/amd64 node:16-alpine

WORKDIR /usr/src/app

COPY ./package*.json ./
RUN npm install
COPY . .

ENV REDIS_SERVER=redis://localhost:6379
ENV SESSION_KEY=OVERRIDE_ME_LOCAL_TESTING_ONLY
ENV APP_VERSION=0.1.2
ENV NODE_ENV=production

RUN echo "fs.inotify.max_user_watchers=1048496" >> /etc/sysctl.conf && sysctl -p

RUN rm -rf /usr/src/app/.git

EXPOSE 3000
CMD [ "npm", "start"]
