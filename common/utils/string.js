/**
 * format user input day, month, year
 * into dateString yyyy-MM-dd 
 * 
 * @param {string} year 
 * @param {string} month 
 * @param {string} day 
 * @returns {string} dateString
 */
const formatDateString = (year,month, day) => {
  return  `${year.trim()}-${month.trim().padStart(2, "0")}-${day.trim().padStart(2, "0")}`
}

module.exports = { formatDateString }