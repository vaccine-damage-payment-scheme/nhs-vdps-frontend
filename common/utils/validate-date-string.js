const { parseISO, isValid } = require('date-fns')
const { formatDateString } = require('./string')

const validateDateString = (day, month, year) => {
    const toISODate = parseISO(formatDateString(year, month, day))
    return isValid(toISODate)
}

module.exports = validateDateString