const winston = require('winston')

const transportsConfig = [new winston.transports.Console()];


function createLogger (level) {
  return winston.createLogger({
    level: level,
    // format: winston.format.json(),
    format: winston.format.combine(
      /* winston.format.label({
             label: `Label🏷️`
        }), */
      winston.format.timestamp({
        format: 'DD-MM-YYYY HH:mm:ss'
      }),
      // winston.format.printf(info => `${info.level}: ${info.label}: ${[info.timestamp]}: ${info.message}`),
      winston.format.printf(info => `${info.level}: ${[info.timestamp]}: ${info.message}`)
    ),
    transports: transportsConfig,
    exceptionHandlers: transportsConfig
  })
}
module.exports = {
  logRequestStart: context => {
    const logger = createLogger(context.level)
    logger.log(context.level, context.message)
    logger.close();
  },

  logRequestEnd: context => {
    const logger = createLogger('info')
    const duration = new Date() - context.startTime
    logger.log('info', `[${context.correlationId}] - ${context.method} to ${context.url} ended - elapsed time: ${duration} ms`)
  },

  logRequestFailure: (context, response) => {
    const logger = createLogger('error')
    logger.log('error', `[${context.correlationId}] Calling ${context.service} to ${context.description} failed -`, {
      service: context.service,
      method: context.method,
      url: context.url,
      status: response.statusCode
    })
  },

  logRequestError: (context, error) => {
    const logger = createLogger('error')
    logger.log('error', `[${context.correlationId}] Calling ${context.service} to ${context.description} threw exception -`, {
      service: context.service,
      method: context.method,
      url: context.url,
      error
    })
  }
}
