const {secretManager} = require('./aws')
const axios = require('axios')
var NotifyClient = require('notifications-node-client').NotifyClient
const htmlentities = require('../assets/htmlentities.json')
const applicationStatusRepository = require('../schemas/ApplicationStatus')
const cdRepository = require('../schemas/ClaimantDetails')
const dataRepository = require('../schemas/Data')
const documentsRepository = require('../schemas/Documents')
const claimRepository = require('../schemas/Claim')
const metaRepository = require('../schemas/MetaData')
const repRepository = require('../schemas/Representative')
const vacRepository = require('../schemas/Vaccinations')
const vnRepository = require('../schemas/VaccineNotes')
const gpRepository = require('../schemas/GeneralPractice')
const hospitalRepository = require('../schemas/Hospitals')
const healthcareRepository = require('../schemas/HealthcareProviders')
const vaccinatedPersonRepository = require('../schemas/VaccinatedPersonDetails')
const disabledPersonRepository = require('../schemas/DisabledPersonDetails')
const healthcareProviderRepository = require('../schemas/HealthcareProviders')
const claimantContactPreferencesRepository = require('../schemas/ClaimantContactPreferences')
const ClaimDisablement = require('../schemas/ClaimDisablement')
const DisabledPersonDetails = require('../schemas/DisabledPersonDetails')
const Documents = require('../../common/schemas/Documents')

/**
 Store claim function
 **/
const storeClaim = async (claimData) => {
  const saveRecordApi = process.argv.local ? process.env.save_record_dynamoDB_api_gateway_endpoint : await secretManager('save_record_dynamoDB_api_gateway_endpoint')
  const saveRecordKey = process.argv.local ? process.env.save_record_dynamoDB_api_gateway_api_key : await secretManager('save_record_dynamoDB_api_gateway_api_key')

  const config = {
    headers: {
      'x-api-key': saveRecordKey,
      'Content-Type': 'application/json',
      Accept: '*/*',
      Connection: 'keep-alive'
    }
  }

  return axios.post(saveRecordApi + '/api/v1/claims/', claimData, config, { ip: '127.0.0.1' })
}

/**
 Verify claim function
 **/
const verifyClaim = async (claimData) => {
  const verificationApi = process.argv.local ? process.env.email_verification_api_gateway_endpoint : await secretManager('email_verification_api_gateway_endpoint')
  const verificationKey = process.argv.local ? process.env.email_verification_api_gateway_api_key : await secretManager('email_verification_api_gateway_api_key')

  const config = {
    headers: {
      'x-api-key': verificationKey,
      'Content-Type': 'application/json',
      Accept: '*/*',
      Connection: 'keep-alive'
    }
  }
  return axios.put(verificationApi + '/api/v1/email-verifications/' + claimData.email, claimData, config)
}

/**
 Get claim function
 **/
const getClaimData = async (email) => {
  const saveRecordApi = process.argv.local ? process.env.save_record_dynamoDB_api_gateway_endpoint : await secretManager('save_record_dynamoDB_api_gateway_endpoint')
  const saveRecordKey = process.argv.local ? process.env.save_record_dynamoDB_api_gateway_api_key : await secretManager('save_record_dynamoDB_api_gateway_api_key')

  const config = {
    headers: {
      'x-api-key': saveRecordKey,
      'Content-Type': 'application/json',
      Accept: '*/*',
      Connection: 'keep-alive'
    }
  }
  return axios.get(saveRecordApi + '/api/v1/claims/' + email, config)
}

/**
 Update claim function
 **/
const updateClaim = async (claimData) => {
  const saveRecordApi = process.argv.local ? process.env.save_record_dynamoDB_api_gateway_endpoint : await secretManager('save_record_dynamoDB_api_gateway_endpoint')
  const saveRecordKey = process.argv.local ? process.env.save_record_dynamoDB_api_gateway_api_key : await secretManager('save_record_dynamoDB_api_gateway_api_key')

  const config = {
    headers: {
      'x-api-key': saveRecordKey,
      'Content-Type': 'application/json',
      Accept: '*/*',
      Connection: 'keep-alive'
    }
  }
  return axios.put(saveRecordApi + '/api/v1/claims/' + claimData.Email, claimData, config)
}

/**
 Verify Email function
 **/
const sendVerificationCode = async (email) => {
  const verificationApi = process.argv.local ? process.env.email_verification_api_gateway_endpoint : await secretManager('email_verification_api_gateway_endpoint')
  const verificationKey = process.argv.local ? process.env.email_verification_api_gateway_api_key : await secretManager('email_verification_api_gateway_api_key')
  const config = {
    headers: {
      'x-api-key': verificationKey,
      'Content-Type': 'application/json',
      Accept: '*/*',
      Connection: 'keep-alive'
    }
  }
  return axios.post(verificationApi + '/api/v1/email-verifications', { email }, config)
}

const notifyClient = async (email, code, expiryDate, expiryTime, reference, key) => {
  const template = process.argv.local ? process.env.gov_notify_service_verification_email_notification_template_id : await secretManager('gov_notify_service_verification_email_notification_template_id');
  const notifyApiKey = process.argv.local ? process.env.gov_notify_service_notification_api_key : await secretManager('gov_notify_service_notification_api_key');
    var client = new NotifyClient(notifyApiKey)
  const notify = await client.sendEmail(template, email, {
    personalisation: {
      'verification code':code,
      'expiry time':expiryTime,
      'expiry date':expiryDate
    },
    reference: reference,
  });
  return notify;
}

const restoreClaim = async (data, req) => {
    //console.log('Data Return: ',data);
    const dataRepo = (await dataRepository()).repository
    const Data = await dataRepo.fetch(req.cookies.sessionID)
    Data.WhyMakingClaim = data.Data?.Claim?.ReasonForClaim || null
    Data.ApplyingFor = data.Data?.Claim?.SelfClaimant
    Data.AccountVerified = true
    Data.NhsNumber = data.Data?.Claim?.NhsNumber || null
    Data.Representative = data.Data?.Representative || null
    await dataRepo.save(Data)

    const vnRepo = (await vnRepository()).repository
    const VaccineNotes = await vnRepo.fetch(req.cookies.sessionID)
    VaccineNotes.Description = data.Data?.Claim?.ClaimDisablement?.Description || null
    VaccineNotes.Diagnosis = data.Data?.Claim?.ClaimDisablement?.Diagnosis || null
    vnRepo.save(VaccineNotes);
    const documentsRepo = (await documentsRepository()).repository
    const Documents = await documentsRepo.fetch(req.cookies.sessionID)


    // Restore Application Status
    const applicationStatusRepo = (await applicationStatusRepository()).repository
    const applicationStatus = await applicationStatusRepo.fetch(req.cookies.sessionID)
    applicationStatus.HealthClinicAttendance = data.Data?.ApplicationStatus?.HealthClinicAttendance || null
    applicationStatus.DoctorDetails = data.Data?.ApplicationStatus?.DoctorDetails || null
    applicationStatus.YourDetails = data.Data?.ApplicationStatus?.YourDetails || null
    applicationStatus.DisabledPersonDetails = data.Data?.ApplicationStatus?.DisabledPersonDetails || null
    applicationStatus.VaccinatedPersonDetails = data.Data?.ApplicationStatus?.VaccinatedPersonDetails || null
    applicationStatus.NominatedPersonDetails = data.Data?.ApplicationStatus?.NominatedPersonDetails || null
    applicationStatus.WhatHappenedNext = data.Data?.ApplicationStatus?.WhatHappenedNext || null
    applicationStatus.DatesOfVaccination = data.Data?.ApplicationStatus?.DatesOfVaccination || null
    applicationStatus.Declaration = data.Data?.ApplicationStatus?.Declaration || null
    applicationStatus.VaccinesReceived = data.Data?.ApplicationStatus?.VaccinesReceived || null
    applicationStatus.HospitalAttendance = data.Data?.ApplicationStatus?.HospitalAttendance || null
    await applicationStatusRepo.save(applicationStatus)

    // Restore Claimant details
    const cdRepo = (await cdRepository()).repository
    const ClaimantDetails = await cdRepo.fetch(req.cookies.sessionID)
    ClaimantDetails.CdTitle = data.Data?.Claim?.ClaimantDetails?.CdTitle || null
    ClaimantDetails.CdSurname = data.Data?.Claim?.ClaimantDetails?.CdSurname || null
    ClaimantDetails.CdMiddlename = data.Data?.Claim?.ClaimantDetails?.CdMiddlename || null
    ClaimantDetails.CdForename = data.Data?.Claim?.ClaimantDetails?.CdForename || null
    ClaimantDetails.CdAddressLine1 = data.Data?.Claim?.ClaimantDetails?.CdAddressLine1 || null
    ClaimantDetails.CdAddressLine2 = data.Data?.Claim?.ClaimantDetails?.CdAddressLine2 || null
    ClaimantDetails.CdTownOrCity = data.Data?.Claim?.ClaimantDetails?.CdTownOrCity || null
    ClaimantDetails.CdCounty = data.Data?.Claim?.ClaimantDetails?.CdCounty || null
    ClaimantDetails.CdPostcode = data.Data?.Claim?.ClaimantDetails?.CdPostcode || null
    ClaimantDetails.CdMobileTelNo = data.Data?.Claim?.ClaimantDetails?.CdMobileTelNo || null
    ClaimantDetails.CdHomeTelNo = data.Data?.Claim?.ClaimantDetails?.CdHomeTelNo || null
    ClaimantDetails.CdWorkTelNo = data.Data?.Claim?.ClaimantDetails?.CdWorkTelNo || null
    ClaimantDetails.CdEmailAddress = data.Data?.Claim?.ClaimantDetails?.CdEmailAddress || null
    ClaimantDetails.CdDateOfBirth = data.Data?.Claim?.ClaimantDetails?.CdDateOfBirth || null
    ClaimantDetails.CdRelationship = data.Data?.Claim?.ClaimantDetails?.CdRelationship || null
    await cdRepo.save(ClaimantDetails)

    // Restore contact preferences
    const claimRepo = (await claimRepository()).repository
    const Claim = await claimRepo.fetch(req.cookies.sessionID)
    Claim.ConsentType = data.Data?.Claim?.ConsentType || null
    Claim.DeclarationA = data.Data?.Claim?.DeclarationA || null
    Claim.DeclarationB = data.Data?.Claim?.DeclarationB || null
    Claim.DeclarationFirstName = data.Data?.Claim?.ClaimantDetails?.DeclarationFirstName || null
    Claim.DeclarationLastName = data.Data?.Claim?.ClaimantDetails?.DeclarationLastName || null
    Claim.ReasonForClaim = data.Data?.Claim?.ReasonForClaim || null
    Claim.ClaimantContactPreferences = data.Data?.Claim?.ClaimantContactPreferences.map(c => JSON.stringify(c)) || null
    await claimRepo.save(Claim)

    // Restore Representative
    const repRepo = (await repRepository()).repository
    const Representative = await repRepo.fetch(req.cookies.sessionID)
    Representative.RepresentativeTitle = data.Data?.Claim?.Representative?.RepresentativeTitle || null
    Representative.RepresentativeSurname = data.Data?.Claim?.Representative?.RepresentativeSurname || null
    Representative.RepresentativeMiddlename = data.Data?.Claim?.Representative?.RepresentativeMiddlename || null
    Representative.RepresentativeForename = data.Data?.Claim?.Representative?.RepresentativeForename || null
    Representative.RepresentativeNino = data.Data?.Claim?.Representative?.RepresentativeNino || null
    Representative.RepresentativeDateOfBirth = data.Data?.Claim?.Representative?.RepresentativeDateOfBirth || null
    Representative.RepresentativeRelationship = data.Data?.Claim?.Representative?.RepresentativeRelationship || null
    Representative.RepresentativeAddressLine1 = data.Data?.Claim?.Representative?.RepresentativeAddressLine1 || null
    Representative.RepresentativeAddressLine2 = data.Data?.Claim?.Representative?.RepresentativeAddressLine2 || null
    Representative.RepresentativeTownOrCity = data.Data?.Claim?.Representative?.RepresentativeTownOrCity || null
    Representative.RepresentativeCounty = data.Data?.Claim?.Representative?.RepresentativeCounty || null
    Representative.RepresentativePostcode = data.Data?.Claim?.Representative?.RepresentativePostcode || null
    Representative.RepresentativeMobileTelNo = data.Data?.Claim?.Representative?.RepresentativeMobileTelNo || null
    Representative.RepresentativeHomeTelNo = data.Data?.Claim?.Representative?.RepresentativeHomeTelNo || null
    Representative.RepresentativeWorkTelNo = data.Data?.Claim?.Representative?.RepresentativeWorkTelNo || null
    Representative.RepresentativeEmailAddress = data.Data?.Claim?.Representative?.RepresentativeEmailAddress || null
    Representative.RepresentativePlaceOfStay = data.Data?.Claim?.Representative?.RepresentativePlaceOfStay || null
    Representative.RepresentativeQuestion = data.Data?.Claim?.Representative?.RepresentativeQuestion || null
    Representative.DisplayedContactInformation = data.Data?.Claim?.Representative?.DisplayedContactInformation || null
    Representative.DisplayedAddress = data.Data?.Claim?.Representative?.DisplayedAddress || null
    await repRepo.save(Representative)

    // Restore Vaccinated Person
    const vdpRepo = (await vaccinatedPersonRepository()).repository
    const VaccinatedPersonDetails = await vdpRepo.fetch(req.cookies.sessionID)
    VaccinatedPersonDetails.VpTitle = data.Data?.Claim?.VaccinatedPersonDetails?.VpTitle || null
    VaccinatedPersonDetails.VpSurname = data.Data?.Claim?.VaccinatedPersonDetails?.VpSurname || null
    VaccinatedPersonDetails.VpMiddlename = data.Data?.Claim?.VaccinatedPersonDetails?.VpMiddlename || null
    VaccinatedPersonDetails.VpForename = data.Data?.Claim?.VaccinatedPersonDetails?.VpForename || null
    VaccinatedPersonDetails.VpNino = data.Data?.Claim?.VaccinatedPersonDetails?.VpNino || null
    VaccinatedPersonDetails.VpAddressLine1 = data.Data?.Claim?.VaccinatedPersonDetails?.VpAddressLine1 || null
    VaccinatedPersonDetails.VpAddressLine2 = data.Data?.Claim?.VaccinatedPersonDetails?.VpAddressLine2 || null
    VaccinatedPersonDetails.VpTownOrCity = data.Data?.Claim?.VaccinatedPersonDetails?.VpTownOrCity || null
    VaccinatedPersonDetails.VpCounty = data.Data?.Claim?.VaccinatedPersonDetails?.VpCounty || null
    VaccinatedPersonDetails.VpPostcode = data.Data?.Claim?.VaccinatedPersonDetails?.VpPostcode || null
    VaccinatedPersonDetails.VpMobileTelNo = data.Data?.Claim?.VaccinatedPersonDetails?.VpMobileTelNo || null
    VaccinatedPersonDetails.VpHomeTelNo = data.Data?.Claim?.VaccinatedPersonDetails?.VpHomeTelNo || null
    VaccinatedPersonDetails.VpWorkTelNo = data.Data?.Claim?.VaccinatedPersonDetails?.VpWorkTelNo || null
    VaccinatedPersonDetails.VpEmailAddress = data.Data?.Claim?.VaccinatedPersonDetails?.VpEmailAddress || null
    VaccinatedPersonDetails.VpDateOfBirth = data.Data?.Claim?.VaccinatedPersonDetails?.VpDateOfBirth || null
    await vdpRepo.save(VaccinatedPersonDetails)

    // Restore Disabled person
    const dpRepo = (await disabledPersonRepository()).repository
    const DisabledPersonDetails = await dpRepo.fetch(req.cookies.sessionID)
    DisabledPersonDetails.DpTitle = data.Data?.Claim?.DisabledPersonDetails?.DpTitle || null
    DisabledPersonDetails.DpSurname = data.Data?.Claim?.DisabledPersonDetails?.DpSurname || null
    DisabledPersonDetails.DpMiddlename = data.Data?.Claim?.DisabledPersonDetails?.DpMiddlename || null
    DisabledPersonDetails.DpForename = data.Data?.Claim?.DisabledPersonDetails?.DpForename || null
    DisabledPersonDetails.DpNino = data.Data?.Claim?.DisabledPersonDetails?.DpNino || null
    DisabledPersonDetails.DpAddressLine1 = data.Data?.Claim?.DisabledPersonDetails?.DpAddressLine1 || null
    DisabledPersonDetails.DpAddressLine2 = data.Data?.Claim?.DisabledPersonDetails?.DpAddressLine2 || null
    DisabledPersonDetails.DpTownOrCity = data.Data?.Claim?.DisabledPersonDetails?.DpTownOrCity || null
    DisabledPersonDetails.DpCounty = data.Data?.Claim?.DisabledPersonDetails?.DpCounty || null
    DisabledPersonDetails.DpPostcode = data.Data?.Claim?.DisabledPersonDetails?.DpPostcode || null
    DisabledPersonDetails.DpMobileTelNo = data.Data?.Claim?.DisabledPersonDetails?.DpMobileTelNo || null
    DisabledPersonDetails.DpHomeTelNo = data.Data?.Claim?.DisabledPersonDetails?.DpHomeTelNo || null
    DisabledPersonDetails.DpWorkTelNo = data.Data?.Claim?.DisabledPersonDetails?.DpWorkTelNo || null
    DisabledPersonDetails.DpEmailAddress = data.Data?.Claim?.DisabledPersonDetails?.DpEmailAddress || null
    DisabledPersonDetails.DpDateOfBirth = data.Data?.Claim?.DisabledPersonDetails?.DpDateOfBirth || null
    DisabledPersonDetails.DpDateOfDeath = data.Data?.Claim?.DisabledPersonDetails?.DpDateOfDeath || null
    await dpRepo.save(DisabledPersonDetails)

    // Restore General Practice
    const gpRepo = (await gpRepository()).repository
    const GeneralPractice = await gpRepo.fetch(req.cookies.sessionID)
    GeneralPractice.DpTitle = data.Data?.Claim?.GeneralPractice?.DpTitle || null
    GeneralPractice.GpDoctorName = data.Data?.Claim?.GeneralPractice?.GpDoctorName || null
    GeneralPractice.GpName = data.Data?.Claim?.GeneralPractice?.GpName || null
    GeneralPractice.GpAddressLine1 = data.Data?.Claim?.GeneralPractice?.GpAddressLine1 || null
    GeneralPractice.GpAddressLine2 = data.Data?.Claim?.GeneralPractice?.GpAddressLine2 || null
    GeneralPractice.GpTownOrCity = data.Data?.Claim?.GeneralPractice?.GpTownOrCity || null
    GeneralPractice.GpCounty = data.Data?.Claim?.GeneralPractice?.GpCounty || null
    GeneralPractice.GpPostcode = data.Data?.Claim?.GeneralPractice?.GpPostcode || null
    GeneralPractice.GpTelNo = data.Data?.Claim?.GeneralPractice?.GpTelNo || null
    GeneralPractice.DisplayedAddress = data.Data?.Claim?.GeneralPractice?.DisplayedAddress || null
    await gpRepo.save(GeneralPractice)

    //console.log('data.Data?.Claim',data.Data?.Claim)
    // Restore MetaData

    const metaRepo = (await metaRepository()).repository
    const MetaData = await metaRepo.fetch(req.cookies.sessionID)
    MetaData.NumberOfVaccines = data.Data?.Claim?.Vaccinations?.length || null
    MetaData.NumberOfHospitals = data.Data?.Claim?.Hospitals?.length || null
    MetaData.NumberOfHealthcareProviders = data.Data?.Claim?.HealthcareProviders?.length || null
    MetaData.DateUnknown = (data.Data?.Claim?.ReasonForClaim === 'polio' && !(data.Data?.Claim?.Vaccinations[0] ? data.Data?.Claim?.Vaccinations[0].VaccinationDate : false)) ? 'Unknown' : false
    await metaRepo.save(MetaData)

    // Restore Vaccination
    const vacRepo = (await vacRepository()).repository
    const vacClient = (await vacRepository()).client
    for(let i = 1; i<= MetaData.NumberOfVaccines; i++){
        let Vaccine = await vacRepo.fetch(req.cookies.sessionID + ":" + i)
        let vaccine = data.Data?.Claim?.Vaccinations[i-1]
        Vaccine.VaccinationType = vaccine?.VaccinationType
        Vaccine.VaccinationDate = vaccine?.VaccinationDate
        Vaccine.VaccinationDisablement = vaccine?.VaccinationDisablement
        Vaccine.VaccinationCountry = vaccine?.VaccinationCountry || 'United Kingdom'
        Vaccine.VaccinationCount = i
        Vaccine.DisplayVaccination = true
        Vaccine.Covid19Type = vaccine?.Covid19Type || null
        await vacRepo.save(Vaccine)
    }


    // Restore Hospitals
    const hospitalRepo = (await hospitalRepository()).repository
    for(let i = 1; i<= data.Data.Claim.Hospitals.length; i++){
         let Hospital = await hospitalRepo.fetch(req.cookies.sessionID + ':' + i)
         let hospital = data.Data?.Claim?.Hospitals[i-1]
         Hospital.HospitalConsultant = hospital.HospitalConsultant || null
         Hospital.HospitalAddressLine1 = hospital.HospitalAddressLine1 || null
         Hospital.HospitalAddressLine2 = hospital.HospitalAddressLine2 || null
         Hospital.HospitalConsultantEmail = hospital.HospitalConsultantEmail || null
         Hospital.HospitalConsultantTelNo = hospital.HospitalConsultantTelNo || null
         Hospital.HospitalCount = i || null
         Hospital.HospitalCounty = hospital.HospitalCounty || null
         Hospital.HospitalDisplayedAddress = true
         Hospital.HospitalName = hospital.HospitalName || null
         Hospital.HospitalPostcode = hospital.HospitalPostcode || null
         Hospital.HospitalTelNo = hospital.HospitalTelNo || null
         Hospital.HospitalTownOrCity = hospital.HospitalTownOrCity || null
         Hospital.DisplayOnline = true
         await hospitalRepo.save(Hospital)
    }

    // Restore Healthcare Providers
    const healthcareRepo = (await healthcareRepository()).repository
    for(let i = 1; i<= data.Data.Claim.HealthcareProviders.length; i++){
        let HealthcareProvider = await healthcareRepo.fetch(req.cookies.sessionID + ':' + i )
        let healthcareProvider = data.Data?.Claim?.HealthcareProviders[i-1]
        HealthcareProvider.ClinicName = healthcareProvider.ClinicName || null
        HealthcareProvider.ClinicAddressLine1 = healthcareProvider.ClinicAddressLine1 || null
        HealthcareProvider.ClinicAddressLine2 = healthcareProvider.ClinicAddressLine2 || null
        HealthcareProvider.ClinicTownOrCity = healthcareProvider.ClinicTownOrCity || null
        HealthcareProvider.ClinicCounty = healthcareProvider.ClinicCounty || null
        HealthcareProvider.ClinicPostcode = healthcareProvider.ClinicPostcode || null
        HealthcareProvider.ClinicTelNo = healthcareProvider.ClinicTelNo || null
        HealthcareProvider.ClinicConsultant = healthcareProvider.ClinicConsultant || null
        HealthcareProvider.ClinicConsultantTelNo = healthcareProvider.ClinicConsultantTelNo || null
        HealthcareProvider.ClinicConsultantEmail = healthcareProvider.ClinicConsultantEmail || null
        HealthcareProvider.ClinicCount = i
        HealthcareProvider.ClinicDisplayedAddress = true
        HealthcareProvider.DisplayOnline = true
        await healthcareRepo.save(HealthcareProvider)
    }
    await resetRedisCacheExpiry(req.sessionID)
}

 const resetRedisCacheExpiry = async function(sessionID) {
        const timeout = 1
   // initiate repos
        const cdRepo = (await cdRepository()).repository
        const dataRepo = (await dataRepository()).repository
        const claimRepo = (await claimRepository()).repository
        const metaRepo = (await metaRepository()).repository
        const repRepo = (await repRepository()).repository
        const vacRepo = (await vacRepository()).repository
        const hospitalRepo = (await hospitalRepository()).repository
        const hpRepo = (await healthcareProviderRepository()).repository
        const appRepo = (await applicationStatusRepository()).repository
        const ccpRepo = (await claimantContactPreferencesRepository()).repository
        const cdisableRepo = (await ClaimDisablement()).repository
        const dpRepo = (await DisabledPersonDetails()).repository
        const docuRepo = (await Documents()).repository
        const gpRepo = (await gpRepository()).repository
        const vnRepo = (await vnRepository()).repository

        // set time to expire seconds
        const ttlInSeconds = 1 * timeout * 60  // 45 minutes

        // set expiry of each repo to expiry time
        await cdRepo.expire(sessionID, ttlInSeconds)
        await dataRepo.expire(sessionID, ttlInSeconds)
        await claimRepo.expire(sessionID, ttlInSeconds)
        await metaRepo.expire(sessionID, ttlInSeconds)
        await repRepo.expire(sessionID, ttlInSeconds)
        await vacRepo.expire(sessionID, ttlInSeconds)
        await hospitalRepo.expire(sessionID, ttlInSeconds)
        await hpRepo.expire(sessionID, ttlInSeconds)
        await appRepo.expire(sessionID, ttlInSeconds)
        await ccpRepo.expire(sessionID, ttlInSeconds)
        await cdisableRepo.expire(sessionID, ttlInSeconds)
        await dpRepo.expire(sessionID, ttlInSeconds)
        await docuRepo.expire(sessionID, ttlInSeconds)
        await gpRepo.expire(sessionID, ttlInSeconds)
        await vnRepo.expire(sessionID, ttlInSeconds)

    return
}

const data = async (req, final = false) => {
    const cdRepo = (await cdRepository()).repository
    const ClaimantDetails = (await cdRepo.fetch(req.cookies.sessionID)).toJSON()

    const dataRepo = (await dataRepository()).repository
    const Data = (await dataRepo.fetch(req.cookies.sessionID)).toJSON()
    
    const claimRepo = (await claimRepository()).repository
    const Claim = await claimRepo.fetch(req.cookies.sessionID)
    
    const metaRepo = (await metaRepository()).repository
    const MetaData = await metaRepo.fetch(req.cookies.sessionID)
    
    const repRepo = (await repRepository()).repository
    const Representative = (await repRepo.fetch(req.cookies.sessionID)).toJSON()

    const vnRepo = (await vnRepository()).repository
    const VaccineNotes = await vnRepo.fetch(req.cookies.sessionID)
    
    const gpRepo = (await gpRepository()).repository
    const GeneralPractice = (await gpRepo.fetch(req.cookies.sessionID)).toJSON()

    const dpRepo = (await disabledPersonRepository()).repository
    const DisabledPersonDetails = (await dpRepo.fetch(req.cookies.sessionID)).toJSON()

    const vpRepo = (await vaccinatedPersonRepository()).repository
    const VaccinatedPersonDetails = (await vpRepo.fetch(req.cookies.sessionID)).toJSON()

    const vacRepo = (await vacRepository()).repository
    const vacClient = (await vacRepository()).client
    Vaccine = await vacRepo.fetch(req.cookies.sessionID + ':' + MetaData.NumberOfVaccines)

    const applicationStatusRepo = (await applicationStatusRepository()).repository
    const applicationStatus = (await applicationStatusRepo.fetch(req.cookies.sessionID)).toJSON()

    const hospitalRepo = (await hospitalRepository()).repository

    const healthcareRepo = (await healthcareRepository()).repository

    const documentsRepo = (await documentsRepository()).repository
    const Documents = (await documentsRepo.fetch(req.cookies.sessionID)).toJSON()

    let fetchedVaccines = []
    let DisplayedVaccines = []
    for(let i=1; i<= MetaData.NumberOfVaccines; i++){
        vaccine = (await vacRepo.fetch(req.cookies.sessionID + ":" + i)).toJSON()
        fetchedVaccines.push(vaccine)
    }

    for(const vaccine of fetchedVaccines){
        if(vaccine.DisplayVaccination){
            delete vaccine.DisplayVaccination
            delete vaccine.SessionID
            delete vaccine.entityId
            delete vaccine.VaccinationCount
            vaccine.VaccinationDisablement = true
            DisplayedVaccines.push(vaccine)
        }
    }


    let Hospitals = []
    for(let i=1; i<= MetaData.NumberOfHospitals; i++){
        hospital = (await hospitalRepo.fetch(req.cookies.sessionID + ":" + i)).toJSON()
        if(hospital.HospitalName){
            if(hospital.DisplayOnline){
                delete hospital.entityId
                delete hospital.DisplayOnline
                delete hospital.HospitalDisplayedAddress
                delete hospital.HospitalCount
                Hospitals.push(hospital)
            }
        }
    }
    let HealthcareProviders = []
    for(let i=1; i<= MetaData.NumberOfHealthcareProviders; i++){
        healthcareProvider = (await healthcareRepo.fetch(req.cookies.sessionID + ":" + i)).toJSON()
        if(healthcareProvider.ClinicName){
            if(healthcareProvider.DisplayOnline){
                delete healthcareProvider.entityId
                delete healthcareProvider.ClinicDisplayedAddress
                delete healthcareProvider.ClinicCount
                delete healthcareProvider.DisplayOnline
                HealthcareProviders.push(healthcareProvider)
            }
        }
    }
    //console.log('req',HealthcareProviders)

    let Files = []
    for(let i=1; i<= MetaData.NumberOfFiles; i++){
        const file = (await documentsRepo.fetch(req.cookies.sessionID + ":" + i)).toJSON()
        if(file.DocumentName){
            delete file.entityId
            delete file.DocumentCount
            delete file.UploadStatus
            Files.push(file)
        }
    }
    const ClaimantDetailsObj = Object.assign({}, ClaimantDetails, {CdEmailAddress: MetaData.Email})
    const DisabledPersonDetailsObj = Object.assign({}, DisabledPersonDetails, {DpEmailAddress: MetaData.Email})
    const VaccinatedPersonDetailsObj = Object.assign({}, VaccinatedPersonDetails, {VpEmailAddress: MetaData.Email})
    delete ClaimantDetailsObj.DisplayedAddress
    delete ClaimantDetailsObj.DisplayedContactInformation
    delete ClaimantDetailsObj.entityId
    delete DisabledPersonDetailsObj.entityId
    delete GeneralPractice.entityId
    delete Representative.entityId
    if(!Representative.RepresentativeCountry){
        Representative.RepresentativeCountry = ''
    }
    const today = new Date();
    const claimData = {
        Submitted: final,
        Email: MetaData.Email,
        Data:removeBlankAttributes({
            "ApplicationStatus": applicationStatus,
            "Claim": removeBlankAttributes({
              "ClaimDate": today.toISOString().split('T')[0],
              "Country": Claim.Country,
              "SelfClaimant": final ? ((Data.ApplyingFor === 'Myself') ? 'Yes' : 'No') : Data.ApplyingFor,
              "ClaimStatus": Data.ClaimStatus,
              "ConsentType": Data.ConsentType || true,
              "ReasonForClaim": Data.WhyMakingClaim,
              "ClaimantContactPreferences": Claim.ClaimantContactPreferences.map(c => JSON.parse(c)),
              "DeclarationA": Claim.DeclarationA,
              "DeclarationB": Claim.DeclarationB || true,
              "DeclarationFirstName": Claim.DeclarationFirstName,
              "DeclarationLastName": Claim.DeclarationLastName,
              "NhsNumber": Data.NhsNumber,
              "ClaimantDetails": removeBlankAttributes(ClaimantDetailsObj),
              "VaccinatedPersonDetails":((Data.WhyMakingClaim === 'mother') || (Data.WhyMakingClaim === 'polio'))?
                removeBlankAttributes({
                        VpTitle: VaccinatedPersonDetailsObj.VpTitle,
                        VpSurname: VaccinatedPersonDetailsObj.VpSurname,
                        VpMiddlename: VaccinatedPersonDetailsObj.VpMiddlename,
                        VpForename: VaccinatedPersonDetailsObj.VpForename,
                        VpNino: VaccinatedPersonDetailsObj.VpNino,
                        VpAddressLine1: VaccinatedPersonDetailsObj.VpAddressLine1,
                        VpAddressLine2: VaccinatedPersonDetailsObj.VpAddressLine2,
                        VpTownOrCity: VaccinatedPersonDetailsObj.VpTownOrCity,
                        VpCounty: VaccinatedPersonDetailsObj.VpCounty,
                        VpPostcode: VaccinatedPersonDetailsObj.VpPostcode,
                        VpMobileTelNo: VaccinatedPersonDetailsObj.VpMobileTelNo,
                        VpHomeTelNo: VaccinatedPersonDetailsObj.VpHomeTelNo,
                        VpWorkTelNo: VaccinatedPersonDetailsObj.VpWorkTelNo,
                        VpEmailAddress: VaccinatedPersonDetailsObj.VpEmailAddress,
                        VpDateOfBirth: VaccinatedPersonDetailsObj.VpDateOfBirth ? (VaccinatedPersonDetailsObj.VpDateOfBirth === '--' ? null : VaccinatedPersonDetailsObj.VpDateOfBirth) : null,
                    })
                : (Data.ApplyingFor === 'Myself') ?
                  removeBlankAttributes({
                      VpTitle: ClaimantDetailsObj.CdTitle,
                      VpSurname: ClaimantDetailsObj.CdSurname,
                      VpMiddlename: ClaimantDetailsObj.CdMiddlename,
                      VpForename: ClaimantDetailsObj.CdForename,
                      VpNino: ClaimantDetailsObj.CdNino,
                      VpAddressLine1: ClaimantDetailsObj.CdAddressLine1,
                      VpAddressLine2: ClaimantDetailsObj.CdAddressLine2,
                      VpTownOrCity: ClaimantDetailsObj.CdTownOrCity,
                      VpCounty: ClaimantDetailsObj.CdCounty,
                      VpPostcode: ClaimantDetailsObj.CdPostcode,
                      VpMobileTelNo: ClaimantDetailsObj.CdMobileTelNo,
                      VpHomeTelNo: ClaimantDetailsObj.CdHomeTelNo,
                      VpWorkTelNo: ClaimantDetailsObj.CdWorkTelNo,
                      VpEmailAddress: ClaimantDetailsObj.CdEmailAddress,
                      VpDateOfBirth: ClaimantDetailsObj.CdDateOfBirth ? (ClaimantDetailsObj.CdDateOfBirth === '--' ? null : ClaimantDetailsObj.CdDateOfBirth) : null,
                  })
                :
                     removeBlankAttributes({
                     VpTitle: DisabledPersonDetailsObj.DpTitle,
                     VpSurname: DisabledPersonDetailsObj.DpSurname,
                     VpMiddlename: DisabledPersonDetailsObj.DpMiddlename,
                     VpForename: DisabledPersonDetailsObj.DpForename,
                     VpNino: DisabledPersonDetailsObj.DpNino,
                     VpAddressLine1: DisabledPersonDetailsObj.DpAddressLine1,
                     VpAddressLine2: DisabledPersonDetailsObj.DpAddressLine2,
                     VpTownOrCity: DisabledPersonDetailsObj.DpTownOrCity,
                     VpCounty: DisabledPersonDetailsObj.DpCounty,
                     VpPostcode: DisabledPersonDetailsObj.DpPostcode,
                     VpMobileTelNo: DisabledPersonDetailsObj.DpMobileTelNo,
                     VpHomeTelNo: DisabledPersonDetailsObj.DpHomeTelNo,
                     VpWorkTelNo: DisabledPersonDetailsObj.DpWorkTelNo,
                     VpEmailAddress: DisabledPersonDetailsObj.DpEmailAddress,
                     VpDateOfBirth: DisabledPersonDetailsObj.DpDateOfBirth ? (DisabledPersonDetailsObj.DpDateOfBirth === '--' ? null : DisabledPersonDetailsObj.DpDateOfBirth) : null,
                     })
                 ,
              "DisabledPersonDetails": (Data.ApplyingFor === 'Myself') ? removeBlankAttributes({
                DpTitle: ClaimantDetailsObj.CdTitle,
                DpSurname: ClaimantDetailsObj.CdSurname,
                DpMiddlename: ClaimantDetailsObj.CdMiddlename,
                DpForename: ClaimantDetailsObj.CdForename,
                DpNino: ClaimantDetailsObj.CdNino,
                DpAddressLine1: ClaimantDetailsObj.CdAddressLine1,
                DpAddressLine2: ClaimantDetailsObj.CdAddressLine2,
                DpTownOrCity: ClaimantDetailsObj.CdTownOrCity,
                DpCounty: ClaimantDetailsObj.CdCounty,
                DpPostcode: ClaimantDetailsObj.CdPostcode,
                DpMobileTelNo: ClaimantDetailsObj.CdMobileTelNo,
                DpHomeTelNo: ClaimantDetailsObj.CdHomeTelNo,
                DpWorkTelNo: ClaimantDetailsObj.CdWorkTelNo,
                DpEmailAddress: ClaimantDetailsObj.CdEmailAddress,
                DpDateOfBirth: ClaimantDetailsObj.CdDateOfBirth,
              }) : removeBlankAttributes(DisabledPersonDetails),
              "Representative": removeBlankAttributes(Representative).RepresentativeSurname ? removeBlankAttributes(Representative) : null,
              "Vaccinations": DisplayedVaccines.map(v => removeBlankAttributes(v)),
              "ClaimDisablement": { Description: VaccineNotes.Description, Diagnosis: VaccineNotes.Diagnosis },
              "GeneralPractice": removeBlankAttributes(GeneralPractice),
              "Hospitals": Hospitals.map(h => removeBlankAttributes(h)),
              "HealthcareProviders": HealthcareProviders.map(hp => removeBlankAttributes(hp)),
              "Documents": Files
            })
        })
    }

    // stringify claimdata so we can search the entire object at once as a string
    let claimDataString = JSON.stringify(claimData)
    for (const [key, value] of Object.entries(htmlentities)) {
        let replaceKey = new RegExp(key, 'gi')
        claimDataString = claimDataString.replace(replaceKey, value)
    }
    return JSON.parse(claimDataString)
}

const removeBlankAttributes = function(obj){
    let o = Object.fromEntries(Object.entries(obj).filter(([_, v]) => v != null));
    return o
}

module.exports = {
  storeClaim,
  verifyClaim,
  getClaimData,
  updateClaim,
  sendVerificationCode,
  notifyClient,
  data,
  restoreClaim
}
