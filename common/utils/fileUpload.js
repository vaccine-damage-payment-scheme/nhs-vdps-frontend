const {secretManager} = require('./aws')
const jwt = require('jsonwebtoken');
const axios = require('axios');
const FormData = require('form-data')
var path = require('path')

    generateJWTToken = async () => {
        let response = {};
        try{
            let now = Date.now();

            let fileServiceClientSecret = await await secretManager('nhsbsa_file_service_client_secret');
            let fileServiceId = await await secretManager('nhsbsa_file_service_service_id');
            if (fileServiceClientSecret && fileServiceId){
                const token = jwt.sign({iss: 'VDPS', iat: Date.now() / 1000}, fileServiceClientSecret);

                response = { "statusCode" : 200, "success" : true, "data" : token };
            }
            else{
                response = { "statusCode" : 400, "success" : false, "data" : "failed to get secrets" };
            }

        }
        catch(err){
            response = { "statusCode" : 400, "success" : false, err };
        }
        return await response;
    }

     generateSecretsAndToken = async () => {
        let response = {};
        try{
            let jwtToken = await generateJWTToken();
            let url = await secretManager('nhsbsa_file_service_url');
            let apiKey = await secretManager('nhsbsa_file_service_api_key');
            let serviceId = await secretManager('nhsbsa_file_service_service_id');
            let userName = await secretManager('nhsbsa_file_service_user_name');

            response = { "statusCode" : 200, "success" : true,
                "data" : {"Token" : jwtToken, "FileUrl" : url, "APIKey" : apiKey , "ServiceId" : serviceId, "UserName" : userName } };
        }
        catch(err){
            response = { "statusCode" : 200, "success" : true, "data" : err};
        }
        return await response;
    }

    async function uploadFile(docReference, docType, file2) {
        let response = {};
        try{
            response = await generateSecretsAndToken();
            if (response.statusCode == 200){
                let formData = new FormData()
                const data = {
                    "applicationrefid": docReference,
                    "doctype":          docType.includes('image') ? 'image' : docType.includes('pdf') ? 'pdf' : docType,
                    "username":         response.data.UserName
                                    }
                const headers = {
                    "Authorization":    `Bearer ${response.data.Token.data}`,
                    "x-api-key":        response.data.APIKey,
                    "Content-Type":     'multipart/form-data',
                    "Accept":           '*/*'
                }
                var unixTimestamp = Math.floor(new Date().getTime()/1000);
                const tempfilename = `${unixTimestamp}-${docReference.replace(/[^a-zA-Z]/g, "")}${path.extname(file2.name)}`
                formData.append('file', file2.data, tempfilename)
                formData.append('serviceid', response.data.ServiceId)
                formData.append('data', JSON.stringify(data))
                let url = response.data.FileUrl + "/files";
                const res = await axios.post(url,formData,{headers})
                response = { "statusCode" : 200, "success" : true, "data" : res.data.filedata };
            }
            else
                response = { "statusCode" : 400, "success" : false, "data" : "failed to get secrets and token" };
        }catch(err){
            response = { "statusCode" : 400, "success" : false, "data" : err };
            console.log(err);
        }
        return await response;
    }

   async function deleteFile(fileId){
        let response = {};
        try{
            response = await generateSecretsAndToken();
            if (response.statusCode == 200){
                const params = {
                    headers: {
                        Authorization: `Bearer ${response.data.Token.data}`,
                        "x-api-key": response.data.APIKey,
                        "Accept":'*/*'
                    }
                };
                url = response.data.FileUrl + "/files/" + fileId;
                const res = await axios.delete(url, params);
                response = { "statusCode" : 200, "success" : true, "data" : res };
            }
            else{
                response = { "statusCode" : 400, "success" : false, "data" : "failed to get secrets" };
            }
        }catch(err){
            response = { "statusCode" : 400, "success" : false, "data" : err };
        }
        return await response;
    }

module.exports = {
    uploadFile: uploadFile,
    deleteFile: deleteFile
};
