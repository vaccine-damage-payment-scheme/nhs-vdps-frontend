const { Entity, Schema } = require('redis-om')
const redisClient = require('./redis-client')

module.exports = async () => {
    class Vaccinations extends Entity {}

    const VaccinationsSchema = new Schema(Vaccinations, {
        VaccinationType: { type: 'string' },
        Covid19Type: { type: 'string' },
        VaccinationDate: { type: 'string' },
        VaccinationDisablement: { type: 'boolean' },
        VaccinationCountry: { type: 'string' },
        VaccinationCount: { type: 'string' },
        SessionID: {type: 'string'},
        DisplayVaccination: {type: 'boolean'}
    })

    const client = await redisClient()
    const repository = client.fetchRepository(VaccinationsSchema)

//    if(repository){
//        await repository.dropIndex()
//        await repository.createIndex()
//    }

  return {repository: repository, client: client}
  client.close()

}
