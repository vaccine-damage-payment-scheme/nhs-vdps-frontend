const { Entity, Schema } = require('redis-om')
const redisClient = require('./redis-client')

module.exports = async () => {
    class Hospitals extends Entity {}

    const HospitalsSchema = new Schema(Hospitals, {
      HospitalConsultant: { type: 'string' },
      HospitalConsultantTelNo: { type: 'string' },
      HospitalConsultantEmail: { type: 'string' },
      HospitalName: { type: 'string' },
      HospitalAddressLine1: { type: 'string' },
      HospitalAddressLine2: { type: 'string' },
      HospitalTownOrCity: { type: 'string' },
      HospitalCounty: { type: 'string' },
      HospitalPostcode: { type: 'string' },
      HospitalTelNo: { type: 'string' },
      HospitalCount: { type: 'number'},
      HospitalDisplayedAddress: { type: 'string' },
      DisplayOnline:{ type: 'boolean' }
    })

    const client = await redisClient()
    const repository = client.fetchRepository(HospitalsSchema)

  return {repository: repository, client: client}
  client.close()

}
