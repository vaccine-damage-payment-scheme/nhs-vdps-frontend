'use strict'
const { Entity, Schema } = require('redis-om')
const redisClient = require('./redis-client')

module.exports = async () => {
  class ClaimantDetails extends Entity {}
  const ClaimantDetailsSchema = new Schema(ClaimantDetails, {
    CdTitle: { type: 'string' },
    CdSurname: { type: 'string' },
    CdMiddlename: { type: 'string' },
    CdForename: { type: 'string' },
    CdNino: { type: 'string' },
    CdAddressLine1: { type: 'string' },
    CdAddressLine2: { type: 'string' },
    CdTownOrCity: { type: 'string' },
    CdCounty: { type: 'string' },
    CdPostcode: { type: 'string' },
    CdMobileTelNo: { type: 'string' },
    CdHomeTelNo: { type: 'string' },
    CdWorkTelNo: { type: 'string' },
    CdEmailAddress: { type: 'string' },
    CdDateOfBirth: { type: 'string' },
    DisplayedContactInformation: { type: 'string' },
    DisplayedAddress: {type: 'string' },
    CdRelationship: {type: 'string'}
  })
  const client = await redisClient()


  const repository = client.fetchRepository(ClaimantDetailsSchema)
//  await repository.createIndex()
  return {repository: repository, client: client}
  client.close()

}

