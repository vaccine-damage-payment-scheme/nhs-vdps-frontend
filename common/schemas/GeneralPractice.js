const { Entity, Schema } = require('redis-om')
const redisClient = require('./redis-client')

module.exports = async () => {
    class GeneralPractice extends Entity {}

    const GeneralPracticeSchema = new Schema(GeneralPractice, {
      GpDoctorName: { type: 'string' },
      GpName: { type: 'string' },
      GpAddressLine1: { type: 'string' },
      GpAddressLine2: { type: 'string' },
      GpTownOrCity: { type: 'string' },
      GpCounty: { type: 'string' },
      GpPostcode: { type: 'string' },
      GpTelNo: { type: 'string' },
      DisplayedAddress: { type: 'string' }
    })
    const client = await redisClient()

    const repository = client.fetchRepository(GeneralPracticeSchema)

  return {repository: repository, client: client}
  client.close()
}
