const { Client } = require('redis-om')
const config = require('../../common/config')
  const redisClient = new Client();

module.exports = async () => {
    if (!redisClient.isOpen()) {
        await redisClient.open(config.REDIS_SERVER);
    }
    return redisClient
}
