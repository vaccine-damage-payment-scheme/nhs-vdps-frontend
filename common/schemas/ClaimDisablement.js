const { Entity, Schema } = require('redis-om')
const redisClient = require('./redis-client')

module.exports = async () => {

class ClaimDisablement extends Entity {}

const ClaimDisablementSchema = new Schema(ClaimDisablement, {
  Description: { type: 'string' },
  Diagnosis: { type: 'string[]' }
})


    const client = await redisClient()

    const repository = client.fetchRepository(ClaimDisablementSchema)

    return { repository: repository, client }
  client.close()
}
