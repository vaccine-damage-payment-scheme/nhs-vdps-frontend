const { Entity, Schema } = require('redis-om')

class ChildSchool extends Entity {}

const ChildSchoolSchema = new Schema(ChildSchool, {
  CsName: { type: 'string' },
  CsAddressLine1: { type: 'string' },
  CsAddressLine2: { type: 'string' },
  CsTownOrCity: { type: 'string' },
  CsCounty: { type: 'string' },
  CsPostcode: { type: 'string' },
  CsTelNo: { type: 'string' }
})

module.exports = () => {
  return {
    schema: ChildSchoolSchema
  }
}
