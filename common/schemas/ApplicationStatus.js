const { Entity, Schema } = require('redis-om')
const redisClient = require('./redis-client')

module.exports = async () => {
    class ApplicationStatus extends Entity {}

    const ApplicationStatusSchema = new Schema(ApplicationStatus, {
        YourDetails: { type: 'string' },
        DisabledPersonDetails: { type: 'string' },
        VaccinatedPersonDetails: { type: 'string' },
        NominatedPersonDetails: { type: 'string' },
        VaccinesReceived: { type: 'string' },
        DatesOfVaccination: { type: 'string' },
        WhatHappenedNext: { type: 'string' },
        DoctorDetails: { type: 'string' },
        HospitalAttendance: { type: 'string' },
        HealthClinicAttendance: { type: 'string' },
        Declaration: { type: 'string' },
        errors: {type: 'boolean'}
    })

    const client = await redisClient()

    const repository = client.fetchRepository(ApplicationStatusSchema)

  return {repository: repository, client: client}
  client.close()
}
