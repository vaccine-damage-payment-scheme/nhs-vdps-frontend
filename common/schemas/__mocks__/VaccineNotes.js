const repository = {
    fetch: jest.fn().mockResolvedValue({}),
    save: jest.fn()
};

module.exports = async () => {
    return {
        repository: repository,
        client: {
            fetchRepository: jest.fn().mockResolvedValue(repository),
            close: jest.fn()
        }
    };
};