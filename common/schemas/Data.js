'use strict'
const { Entity, Schema } = require('redis-om')
const redisClient = require('./redis-client')

module.exports = async () => {

  class Data extends Entity {}
  const DataSchema = new Schema(Data, {
    ApplicationStatus: { type: 'string' },
    Claim: { type: 'string' },
    ApplyingFor: { type: 'string' },
    ConsentType: { type: 'string' },
    PreviousClaim: { type: 'string' },
    WhyMakingClaim: { type: 'string' },
    NhsNumber: { type: 'string' },
    AccountVerified: { type: 'boolean' },
    Representative: { type: 'string' },
    VisitedHospital: { type: 'string' },
    VisitedHealthProvider: { type: 'string' }
  })
  const client = await redisClient()


  const repository = client.fetchRepository(DataSchema)
  return {repository: repository, client: client}
  client.close()

}

