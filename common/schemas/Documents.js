const { Entity, Schema } = require('redis-om')
const redisClient = require('./redis-client')

module.exports = async () => {

    class Documents extends Entity {}

    const DocumentsSchema = new Schema(Documents, {
      DocumentName: { type: 'string' },
      DocumentID: { type: 'string' },
      DocumentType: { type: 'string' },
      DocumentPath: { type: 'string' },
      DocumentStatus: { type: 'string' },
      DocumentCount: { type: 'number'},
      UploadStatus: {type: 'string'}
    })
    const client = await redisClient()

    const repository = client.fetchRepository(DocumentsSchema)

  return {repository: repository, client: client}
  client.close()
}
