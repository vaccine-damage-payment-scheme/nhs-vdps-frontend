'use strict'
const { Entity, Schema } = require('redis-om')
const redisClient = require('./redis-client')

/* create the index for Person */
module.exports = async () => {
  class MetaData extends Entity {

  }
  const MetaDataSchema = new Schema(MetaData, {
    Email: { type: 'string' },
    Email: { type: 'string' },
    Submitted: { type: 'boolean' },
    ExpirateTime: { type: 'string' },
    Data: { type: 'string' },
    NumberOfVaccines: { type: 'number' },
    NumberOfHospitals: { type: 'number' },
    NumberOfHealthcareProviders: { type: 'number' },
    NumberOfFiles: { type: 'number' },
    DisplayedVaccineCount: {type: 'number'},
    ResendCodeCount: {type: 'number'},
    DateUnknown: { type: 'string' }
  })

  const client = await redisClient()

  const repository = client.fetchRepository(MetaDataSchema)

  return {repository: repository, client: client}
  client.close()

}
