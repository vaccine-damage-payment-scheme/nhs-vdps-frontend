module.exports = {
      clearMocks: true,
      moduleDirectories: ['node_modules'],
      testEnvironment: 'node',
      testMatch: ["**/*.spec.js"],
      verbose: true,
      setupFilesAfterEnv: ['<rootDir>/test/jest.setup.js'],
  };
  